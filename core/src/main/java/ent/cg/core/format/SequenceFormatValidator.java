package ent.cg.core.format;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SequenceFormatValidator implements ConstraintValidator<SequenceFormat,Object> {

    private String format;

    @Override
    public void initialize(SequenceFormat constraintAnnotation) {
        this.format = constraintAnnotation.format();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        var str = (String)o;
        return str.matches(this.format);
    }
}
