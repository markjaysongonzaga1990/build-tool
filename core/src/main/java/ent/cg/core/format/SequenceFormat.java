package ent.cg.core.format;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = SequenceFormatValidator.class)
@Documented
public @interface SequenceFormat {
    String message() default """
            Format is incorrect. 
            Should be not greater than 10 digits but not less than 5 digit numbers. 
            Can also have leading zeros if needed""";

    String format() default "^[0-9][0-9]{4,10}$";

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
