package ent.cg.core.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ent.cg.core.enums.COMPANY_TYPE;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PROTECTED)
public abstract class BaseEvent {

    @JsonIgnore
    COMPANY_TYPE companyType;

    @JsonIgnore
    UUID user;

   public String payload(){
       return this.toString();
   }

   public String eventName(){
       return getClass().getSimpleName();
   }
}
