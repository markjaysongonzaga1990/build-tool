package ent.cg.core.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.core.enums.DOCUMENT_TYPE;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@RegisterForReflection
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookletReleaseEvent extends BaseEvent{
    @NotNull(message="Booklet Number should not be empty")
    @Min(message="Booklet count should not be 0", value=1)
    @Max(message = "Booklet count should not be greater than 5",value = 5)
    int bookletCount;

    @NotNull(message="Branch Office should not be empty")
    Long branchOfficeId;

    @NotNull(message="Assigned Personnel should not be empty")
    Long assignedPersonnelId;

    @NotNull(message = "Document Type is required.")
    DOCUMENT_TYPE documentType;

    @JsonIgnore
    Long sequencePcs = 50L;
}
