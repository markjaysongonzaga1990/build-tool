package ent.cg.core.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ent.cg.core.enums.DOCUMENT_TYPE;
import ent.cg.core.enums.DOCUMENT_UNIT;
import ent.cg.core.format.SequenceFormat;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@RegisterForReflection
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SequenceRegisterEvent extends BaseEvent{
    @SequenceFormat
    @NotEmpty(message="Starting Range should not be empty")
    String sequenceFrom;

    @SequenceFormat
    @NotEmpty(message="Ending Range should not be empty")
    String sequenceTo;

    @NotNull(message = "Document Type is required.")
    DOCUMENT_TYPE documentType;

    @JsonIgnore
    DOCUMENT_UNIT documentUnit;

}
