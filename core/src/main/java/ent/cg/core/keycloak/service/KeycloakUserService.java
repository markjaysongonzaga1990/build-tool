package ent.cg.core.keycloak.service;

import ent.cg.core.dto.UserQuery;
import io.quarkus.cache.CacheResult;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.representations.idm.UserRepresentation;

import javax.inject.Singleton;
import java.util.UUID;

@Slf4j
@Singleton
public class KeycloakUserService {

    @ConfigProperty(name = "quarkus.oidc.auth-server-url")
    String SERVER_URL;

    @CacheResult(cacheName = "keycloak-user-cache")
    public UserQuery retrieveName(UUID subject){
        log.info("Calling keycloak server :: {}",SERVER_URL);
        var userRepresentation = getUserRepresentation(subject);
        var name = String.join(" ", userRepresentation.getFirstName(), userRepresentation.getLastName());
        log.info("Name found :: {}",name);
        return UserQuery.builder()
                .sub(subject)
                .fullName(name)
                .email(userRepresentation.getEmail())
                .build();
    }

    @CacheResult(cacheName = "keycloak-user-email-cache")
    public String retrieveUserEmail(UUID subject){
        log.info("Calling keycloak server for retrieving email:: {}",SERVER_URL);
        var userRepresentation = getUserRepresentation(subject);
        return userRepresentation.getEmail();
    }

    private UserRepresentation getUserRepresentation(UUID subject) {
        var keycloak = keycloakInstance();
        var usersResource = keycloak.realm("app").users();
        return usersResource.get(String.valueOf(subject)).toRepresentation();
    }

    public Keycloak keycloakInstance() {
        return KeycloakBuilder.builder()
                .serverUrl(SERVER_URL.replace("/realms/app", ""))
                .realm("app")
                .clientId("admin-cli")
                .username("keycloakuser")
                .password("keycloakpassword")
                .grantType(OAuth2Constants.PASSWORD)
                .build();
    }
}
