package ent.cg.core.dto;

import ent.cg.core.enums.TRANSACTION_TYPE;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@RegisterForReflection
@FieldDefaults(level = AccessLevel.PRIVATE)
public class KeyHolder {
    TRANSACTION_TYPE transactionType;
    UUID key;
}
