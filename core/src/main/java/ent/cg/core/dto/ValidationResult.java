package ent.cg.core.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RegisterForReflection
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ValidationResult {
    List<String> message;
    Boolean success = false;

    public ValidationResult(Set<? extends ConstraintViolation<?>> violations) {
        this.message = violations.stream().map((cv) -> cv.getMessage())
                .collect(Collectors.toList());
    }
}
