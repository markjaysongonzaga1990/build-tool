package ent.cg.core.dto;

import ent.cg.core.enums.TRANSACTION_RESULT;
import ent.cg.core.enums.TRANSACTION_TYPE;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@RegisterForReflection
@FieldDefaults(level = AccessLevel.PRIVATE)
public class NotificationMessage {
    String message;
    String event;
    TRANSACTION_TYPE transactionType;
    TRANSACTION_RESULT result;
}
