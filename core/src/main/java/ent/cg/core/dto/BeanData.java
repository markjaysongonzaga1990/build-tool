package ent.cg.core.dto;

import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.core.enums.TRANSACTION_TYPE;
import ent.cg.core.enums.TRANSACTION_RESULT;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.UUID;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RegisterForReflection
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BeanData implements Serializable {
    COMPANY_TYPE companyType;
    TRANSACTION_TYPE transactionType;
    TRANSACTION_RESULT transactionResult;
    String event;
    String message;
    UUID user;
}
