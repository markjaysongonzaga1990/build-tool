package ent.cg.core.dto;

import ent.cg.core.enums.TRANSACTION_TYPE;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import javax.websocket.Session;

@Data
@Builder
@AllArgsConstructor
@RegisterForReflection
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SessionHolder {
    TRANSACTION_TYPE transactionType;
    Session session;
}
