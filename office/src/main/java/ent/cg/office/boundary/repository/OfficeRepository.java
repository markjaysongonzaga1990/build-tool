package ent.cg.office.boundary.repository;

import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.office.boundary.Office;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Parameters;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class OfficeRepository implements PanacheRepository<Office> {

    public List<Office> findByCompany(COMPANY_TYPE company, Page page){
        var parameters = Parameters.with("company",company);
        return find("FROM Office o WHERE o.company =: company", parameters)
                .page(page).list();
    }

}
