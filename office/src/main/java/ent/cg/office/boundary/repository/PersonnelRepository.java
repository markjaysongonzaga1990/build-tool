package ent.cg.office.boundary.repository;

import ent.cg.office.boundary.Office;
import ent.cg.office.boundary.Personnel;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class PersonnelRepository implements PanacheRepository<Personnel> {

    public List<Personnel> findByOffice(Office office){
        var params = Parameters.with("office", office);
        return find("FROM Personnel p WHERE p.office =: office", params).list();
    }

}
