package ent.cg.office.boundary;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Personnel {

    @Id
    @GeneratedValue
    Long id;

    String lastName;
    String firstName;
    String middleName;

    @Column(unique = true)
    String emailAddress;

    @OneToMany(cascade = CascadeType.ALL)
    List<Designation> designations;

    @OneToMany(cascade = CascadeType.REFRESH)
    List<Office> offices;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(nullable = false)
    UUID addedBy;
}
