package ent.cg.office.boundary.repository;

import ent.cg.office.boundary.Designation;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class DesignationRepository implements PanacheRepository<Designation> {
}
