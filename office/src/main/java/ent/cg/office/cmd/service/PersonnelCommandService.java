package ent.cg.office.cmd.service;

import ent.cg.core.keycloak.service.KeycloakUserService;
import ent.cg.office.boundary.Personnel;
import ent.cg.office.boundary.repository.DesignationRepository;
import ent.cg.office.boundary.repository.OfficeRepository;
import ent.cg.office.boundary.repository.PersonnelRepository;
import ent.cg.office.cmd.event.AddPersonnelEvent;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.stream.Collectors;

@Slf4j
@ApplicationScoped
public class PersonnelCommandService {

    @Inject
    KeycloakUserService keycloakUserService;

    @Inject
    PersonnelRepository personnelRepository;

    @Inject
    OfficeRepository officeRepository;

    @Inject
    DesignationRepository designationRepository;

    @Transactional
    public void addPersonnelEvent(AddPersonnelEvent addPersonnelEvent) {
        var office = this.officeRepository.findByIdOptional(addPersonnelEvent.getOfficeId())
                .orElseThrow(() -> new RuntimeException("Office not found!"));

        var designations = addPersonnelEvent.getDesignationsId()
                .parallelStream()
                .map(x-> this.designationRepository.findById(x))
                .collect(Collectors.toList());

        if(designations.size() <=0) throw new RuntimeException("No designation found!");

        this.personnelRepository.persist(Personnel.builder()
                .addedBy(addPersonnelEvent.getAddedBy())
                .emailAddress(addPersonnelEvent.getEmailAddress())
                .firstName(addPersonnelEvent.getFirstName())
                .lastName(addPersonnelEvent.getLastName())
                .middleName(addPersonnelEvent.getMiddleName())
                .designations(designations)
                .offices(Arrays.asList(office))
                .build());

        //then add to keycloak
        this.addKeycloakUser(addPersonnelEvent);
    }

    public void addKeycloakUser(AddPersonnelEvent addPersonnelEvent){
        log.info("Adding user to keycloak");
        var companyName = addPersonnelEvent.getCompanyType().name();

        // Create user (requires manage-users role)
        var keycloak = keycloakUserService.keycloakInstance();

        var keycloakUser = new UserRepresentation();
        keycloakUser.setUsername(addPersonnelEvent.getFirstName());
        keycloakUser.setFirstName(addPersonnelEvent.getFirstName());
        keycloakUser.setLastName(addPersonnelEvent.getLastName());
        keycloakUser.setEmail(addPersonnelEvent.getEmailAddress());
        keycloakUser.setEmailVerified(false);
        keycloakUser.setEnabled(false);

        keycloakUser.setRealmRoles(Arrays.asList(companyName));

        var realmResource  = keycloak.realm("app");
        var usersResource = realmResource.users();

        var response = usersResource.create(keycloakUser);
        var userId = CreatedResponseUtil.getCreatedId(response);

        //update password
        var userResource = usersResource.get(userId);
        var credential = new CredentialRepresentation();
        credential.setType(CredentialRepresentation.PASSWORD);
        credential.setValue("keycloakPassword");
        userResource.resetPassword(credential);

        //add realm role
        var companyRole = realmResource.roles().get(companyName).toRepresentation();
        userResource.roles().realmLevel()
                .add(Arrays.asList(companyRole));

        //send password via-email
        //userResource.executeActionsEmail(Arrays.asList("UPDATE_PASSWORD","VERIFY_EMAIL"));
    }
}
