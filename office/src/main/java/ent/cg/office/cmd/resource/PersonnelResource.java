package ent.cg.office.cmd.resource;

import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.office.cmd.event.AddPersonnelEvent;
import io.vertx.mutiny.core.eventbus.EventBus;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.xml.validation.Validator;
import java.util.UUID;

@Path("/personnel")
public class PersonnelResource {

    @Inject
    EventBus eventBus;

    @Inject
    JsonWebToken jwt;

    @POST
    @Path("/{company}")
    public Response addOffice(@PathParam("company") COMPANY_TYPE companyType,
                              AddPersonnelEvent addPersonnelEvent){
        addPersonnelEvent.setCompanyType(companyType);
        addPersonnelEvent.setAddedBy(UUID.fromString(jwt.getSubject()));
        this.eventBus.send("addPersonnelEvent",addPersonnelEvent);
        return Response.accepted().build();
    }

}
