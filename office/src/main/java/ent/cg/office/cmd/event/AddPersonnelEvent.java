package ent.cg.office.cmd.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.office.boundary.Designation;
import ent.cg.office.boundary.Office;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RegisterForReflection
public class AddPersonnelEvent {

    String lastName;
    String firstName;
    String middleName;

    String emailAddress;

    List<Long> designationsId;

    Long officeId;

    @JsonIgnore
    COMPANY_TYPE companyType;

    @JsonIgnore
    UUID addedBy;
}
