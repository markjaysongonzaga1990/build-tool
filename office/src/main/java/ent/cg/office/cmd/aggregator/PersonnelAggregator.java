package ent.cg.office.cmd.aggregator;

import ent.cg.office.cmd.event.AddPersonnelEvent;
import ent.cg.office.cmd.service.PersonnelCommandService;
import io.quarkus.vertx.ConsumeEvent;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PersonnelAggregator {

    @Inject
    PersonnelCommandService personnelCommandService;

    @ConsumeEvent(value = "addPersonnelEvent",blocking = true)
    public void on(AddPersonnelEvent addPersonnelEvent){
        this.personnelCommandService.addPersonnelEvent(addPersonnelEvent);
    }

}
