package ent.cg.office.cmd.service;

import ent.cg.office.boundary.Office;
import ent.cg.office.boundary.repository.OfficeRepository;
import ent.cg.office.cmd.event.AddOfficeEvent;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@ApplicationScoped
public class OfficeCommandService {
    @Inject
    OfficeRepository officeRepository;

    @Transactional
    public void addOfficeEvent(AddOfficeEvent addOfficeEvent) {
        this.officeRepository.persist(Office.builder()
                .address(addOfficeEvent.getAddress())
                .company(addOfficeEvent.getCompanyType())
                .addedBy(addOfficeEvent.getAddedBy())
                .build());
    }
}
