package ent.cg.office.cmd.aggregator;

import ent.cg.office.cmd.event.AddDesignationEvent;
import ent.cg.office.cmd.event.AddOfficeEvent;
import ent.cg.office.cmd.service.DesignationCommandService;
import ent.cg.office.cmd.service.OfficeCommandService;
import io.quarkus.vertx.ConsumeEvent;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class DesignationAggregator {

    @Inject
    DesignationCommandService designationCommandService;

    @ConsumeEvent(value = "addDesignationEvent",blocking = true)
    public void on(AddDesignationEvent addDesignationEvent){
        this.designationCommandService.addDesignationEvent(addDesignationEvent);
    }

}
