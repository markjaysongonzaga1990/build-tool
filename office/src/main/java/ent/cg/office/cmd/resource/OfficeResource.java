package ent.cg.office.cmd.resource;

import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.office.cmd.event.AddOfficeEvent;
import io.vertx.mutiny.core.eventbus.EventBus;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.xml.validation.Validator;
import java.util.UUID;

@Path("/office")
public class OfficeResource {

    @Inject
    EventBus eventBus;

    @Inject
    JsonWebToken jwt;

    @POST
    @Path("/{company}")
    public Response addOffice(@PathParam("company") COMPANY_TYPE companyType,
                              AddOfficeEvent addOfficeEvent){
        addOfficeEvent.setCompanyType(companyType);
        addOfficeEvent.setAddedBy(UUID.fromString(jwt.getSubject()));
        this.eventBus.send("addOfficeEvent",addOfficeEvent);
        return Response.accepted().build();
    }

}
