package ent.cg.office.cmd.service;

import ent.cg.office.boundary.Designation;
import ent.cg.office.boundary.repository.DesignationRepository;
import ent.cg.office.cmd.event.AddDesignationEvent;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@ApplicationScoped
public class DesignationCommandService {
    @Inject
    DesignationRepository designationRepository;


    @Transactional
    public void addDesignationEvent(AddDesignationEvent addDesignationEvent) {
        this.designationRepository.persist(Designation.builder()
                .addedBy(addDesignationEvent.getAddedBy())
                .description(addDesignationEvent.getDescription())
                .position(addDesignationEvent.getPosition())
                .build());
    }
}
