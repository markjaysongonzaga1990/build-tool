package ent.cg.office.cmd.aggregator;

import ent.cg.office.cmd.event.AddOfficeEvent;
import ent.cg.office.cmd.service.OfficeCommandService;
import io.quarkus.vertx.ConsumeEvent;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class OfficeAggregator {

    @Inject
    OfficeCommandService officeCommandService;

    @ConsumeEvent(value = "addOfficeEvent",blocking = true)
    public void on(AddOfficeEvent addOfficeEvent){
        this.officeCommandService.addOfficeEvent(addOfficeEvent);
    }

}
