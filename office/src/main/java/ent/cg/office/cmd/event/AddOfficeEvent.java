package ent.cg.office.cmd.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ent.cg.core.enums.COMPANY_TYPE;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RegisterForReflection
public class AddOfficeEvent {

    String address;

    @JsonIgnore
    COMPANY_TYPE companyType;

    @JsonIgnore
    UUID addedBy;
}
