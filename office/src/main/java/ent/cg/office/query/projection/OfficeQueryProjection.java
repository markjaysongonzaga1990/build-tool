package ent.cg.office.query.projection;

import ent.cg.core.dto.UserQuery;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RegisterForReflection
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OfficeQueryProjection {
    Long id;
    String address;

    List<UserQueryProjection> personnels;
    UserQuery addedBy;
    Date createdDate;
}
