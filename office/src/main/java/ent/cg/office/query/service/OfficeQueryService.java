package ent.cg.office.query.service;

import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.core.keycloak.service.KeycloakUserService;
import ent.cg.office.boundary.repository.OfficeRepository;
import ent.cg.office.boundary.repository.PersonnelRepository;
import ent.cg.office.query.projection.OfficeQueryProjection;
import ent.cg.office.query.projection.UserQueryProjection;
import io.quarkus.panache.common.Page;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class OfficeQueryService {

    @Inject
    KeycloakUserService keycloakUserService;

    @Inject
    OfficeRepository officeRepository;

    @Inject
    PersonnelRepository personnelRepository;

    public List<OfficeQueryProjection> findByCompanyAndDesignation(COMPANY_TYPE companyType,
                                                     String designation,
                                                     Integer size,
                                                     Integer page) {
        var offices = this.officeRepository.findByCompany(companyType, Page.of(page, size));
        return offices.parallelStream()
                .map(x-> {
                    var userName = this.keycloakUserService.retrieveName(x.getAddedBy());
                    var personnelList = this.personnelRepository.findByOffice(x)
                            .parallelStream()
                            .filter(personnel -> personnel.getDesignations()
                                    .stream()
                                    .filter(a-> a.getPosition().equalsIgnoreCase(designation))
                                    .findAny().isPresent())
                            .map(p-> UserQueryProjection.builder()
                                    .id(p.getId())
                                    .designations(Arrays.asList(designation))
                                    .firstName(p.getFirstName())
                                    .lastName(p.getLastName())
                                    .middleName(p.getMiddleName())
                                    .emailAddress(p.getEmailAddress())
                                    .build())
                            .collect(Collectors.toList());

                    var officeQueryProjection = OfficeQueryProjection.builder()
                            .id(x.getId())
                            .address(x.getAddress())
                            .personnels(personnelList)
                            .createdDate(x.getCreatedDate())
                            .addedBy(userName)
                            .build();
                    return officeQueryProjection;
                })
                .collect(Collectors.toList());
    }
}
