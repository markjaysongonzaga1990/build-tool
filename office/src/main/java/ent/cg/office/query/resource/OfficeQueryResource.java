package ent.cg.office.query.resource;

import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.office.cmd.event.AddDesignationEvent;
import ent.cg.office.query.service.OfficeQueryService;
import io.vertx.mutiny.core.eventbus.EventBus;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.xml.validation.Validator;
import java.util.UUID;

@Path("/query/office")
public class OfficeQueryResource {

    @Inject
    OfficeQueryService officeQueryService;

    @GET
    @Path("/{company}/{designation}")
    public Response fetchByCompany(@DefaultValue("20") @QueryParam ("size")Integer size,
                                   @DefaultValue ("0")@QueryParam("page") Integer page,
                                   @PathParam("company") COMPANY_TYPE companyType,
                                   @PathParam("designation") String designation){
        var list = officeQueryService.findByCompanyAndDesignation(companyType,designation,size,page);
        return Response.ok(list).build();
    }

}
