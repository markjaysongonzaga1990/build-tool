package ent.cg.office.query.projection;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RegisterForReflection
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserQueryProjection {
    Long id;
    String firstName;
    String lastName;
    String middleName;
    String emailAddress;

    List<String> designations;
}
