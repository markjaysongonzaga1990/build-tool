package ent.cg.notification.entity.repository;

import ent.cg.notification.entity.Alert;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Parameters;
import io.quarkus.panache.common.Sort;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.UUID;

@ApplicationScoped
public class AlertRepository implements PanacheRepository<Alert> {

    public List<Alert> findByUserId(UUID userId,Integer size) {
        return find("FROM Alert e WHERE e.userId =:userId",
                Sort.descending("id"),
                Parameters.with("userId", userId))
                .page(Page.ofSize(size))
                .list();
    }
}
