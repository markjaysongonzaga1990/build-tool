package ent.cg.notification.entity;

import ent.cg.core.dto.BeanData;
import ent.cg.notification.hibernate.MyJsonType;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@TypeDef(name = "MyJsonType",typeClass = MyJsonType.class)
public class Alert {
    @Id
    @GeneratedValue
    Long id;

    @Type(type = "MyJsonType")
    BeanData beanData;

    UUID userId;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;
}
