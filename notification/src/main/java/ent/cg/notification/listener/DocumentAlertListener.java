package ent.cg.notification.listener;

import ent.cg.core.dto.BeanData;
import ent.cg.notification.service.AlertService;
import io.vertx.core.json.JsonObject;
import io.vertx.mutiny.core.eventbus.EventBus;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@Slf4j
@ApplicationScoped
public class DocumentAlertListener {

    @Inject
    AlertService alertService;

    @Inject
    EventBus eventBus;

    @Incoming("document-notification")
    public void documentIn(JsonObject jsonObject) {
        log.info("Received Notification");
        var metaData = jsonObject.mapTo(BeanData.class);
        this.eventBus.send("saveAlert",metaData);
    }

}
