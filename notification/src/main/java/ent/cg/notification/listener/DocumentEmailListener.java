package ent.cg.notification.listener;

import ent.cg.core.dto.BeanData;
import ent.cg.notification.service.EmailService;
import io.vertx.core.json.JsonObject;
import io.vertx.mutiny.core.eventbus.EventBus;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@Slf4j
@ApplicationScoped
public class DocumentEmailListener {

    @Inject
    EventBus eventBus;

    @Incoming("document-email")
    public void documentIn(JsonObject payload) {
        var metaData = payload.mapTo(BeanData.class);
        eventBus.send("documentEmail",metaData);
    }
}
