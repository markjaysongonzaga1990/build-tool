package ent.cg.notification.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ent.cg.core.dto.NotificationMessage;
import ent.cg.notification.entity.Alert;
import ent.cg.notification.service.AlertService;
import io.quarkus.scheduler.Scheduled;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.websocket.Session;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Data
@Slf4j
@Singleton
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WSCache {
    Map<UUID, Session> sessions = new ConcurrentHashMap<>();

    @Inject
    AlertService alertService;

    public void addSession(UUID key, Session session) {
        log.info("Adding key and session :: {}",key);
        this.sessions.put(key,session);
    }

    public void removeSession(UUID key) {
        log.info("Removing session");
        this.sessions.remove(key);
    }

    @Scheduled(every="10s")
    public void sendNotification(){
        log.info("Sending Notifications to :: {} sessions ",sessions.size());
        sessions.keySet()
                .parallelStream()
                .peek(uuid-> log.info("Processing :: {}",uuid))
                .forEach(userId->{
                    var session = sessions.get(userId);
                    var notificationList = this.alertService.fetchNotificationByUserId(userId)
                            .parallelStream()
                            .map(Alert::getBeanData)
                            .map(metaData-> {
                                var message = metaData.getMessage();
                                var event = metaData.getEvent();
                                var transactionType = metaData.getTransactionType();
                                var transactionResult = metaData.getTransactionResult();
                                return NotificationMessage.builder()
                                        .message(message)
                                        .event(event)
                                        .transactionType(transactionType)
                                        .result(transactionResult)
                                        .build();
                            }).collect(Collectors.toList());

                    try {
                        var jsonMapper = new ObjectMapper().writeValueAsString(notificationList);
                        log.info("Sending :: {} :: {}",userId,jsonMapper);
                        session.getAsyncRemote()
                                .sendObject(jsonMapper, sendResult -> {
                                    var exception = sendResult.getException();
                                    if(Optional.ofNullable(exception).isPresent()){
                                        log.error("ERROR :: {}", exception.getMessage());
                                        throw new RuntimeException(exception);
                                    }
                                });
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                });
    }
}
