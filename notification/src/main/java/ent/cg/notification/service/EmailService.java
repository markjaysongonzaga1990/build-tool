package ent.cg.notification.service;

import ent.cg.core.dto.BeanData;
import ent.cg.core.keycloak.service.KeycloakUserService;
import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;
import io.quarkus.vertx.ConsumeEvent;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@Slf4j
@ApplicationScoped
public class EmailService {

    @Inject
    Mailer mailer;

    @Inject
    KeycloakUserService keycloakUserService;

    @ConsumeEvent(value = "documentEmail",blocking = true)
    public void documentEmail(BeanData metaData){
        log.info("Sending email");
        var userEmail = this.keycloakUserService.retrieveUserEmail(metaData.getUser());
        var event = metaData.getEvent();
        var message = "Your " + event + " transaction is " + metaData.getTransactionResult().name();
        this.mailer.send(
                Mail.withText(userEmail, event, message)
        );
    }

}
