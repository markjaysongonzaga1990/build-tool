package ent.cg.notification.service;

import ent.cg.core.dto.BeanData;
import ent.cg.notification.entity.Alert;
import ent.cg.notification.entity.repository.AlertRepository;
import io.quarkus.vertx.ConsumeEvent;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Slf4j
@ApplicationScoped
public class AlertService {

    @Inject
    AlertRepository alertRepository;

    @Transactional
    @ConsumeEvent(value = "saveAlert",blocking = true)
    public void saveNotification(BeanData metaData) {
        this.alertRepository.persist(Alert.builder()
                .beanData(metaData)
                .userId(metaData.getUser())
                .build());
    }

    public List<Alert> fetchNotificationByUserId(UUID userId){
        return this.alertRepository.findByUserId(userId,5);
    }
}
