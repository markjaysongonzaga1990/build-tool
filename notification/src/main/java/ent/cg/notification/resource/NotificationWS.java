package ent.cg.notification.resource;

import ent.cg.notification.config.WSCache;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.UUID;

@Slf4j
@ApplicationScoped
@ServerEndpoint("/notify/{key}")
public class NotificationWS {

    @Inject
    WSCache wsCache;

    @OnOpen
    public void onOpen(Session session, @PathParam("key") String key) {
        wsCache.addSession(convertKeyToUUID(key), session);
    }

    @OnClose
    public void onClose(Session session, @PathParam("key") String key) {
        wsCache.removeSession(convertKeyToUUID(key));
    }

    @OnError
    public void onError(Session session, @PathParam("key") String key, Throwable throwable) {
        wsCache.removeSession(convertKeyToUUID(key));
        throwable.printStackTrace();
    }

    private final UUID convertKeyToUUID(@PathParam("key") String key) {
        return UUID.fromString(key);
    }
}
