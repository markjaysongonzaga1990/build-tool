package ent.cg.cmd.service.registry;

import ent.cg.boundary.Document;
import ent.cg.boundary.Registry;
import ent.cg.boundary.repository.RegistryRepository;
import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.core.enums.DOCUMENT_TYPE;
import ent.cg.core.enums.DOCUMENT_UNIT;
import ent.cg.core.event.BookletRegisterEvent;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.ConfigMetadata;
import io.quarkus.test.security.oidc.OidcSecurity;
import io.quarkus.test.security.oidc.UserInfo;
import io.smallrye.reactive.messaging.providers.connectors.InMemoryConnector;
import org.junit.jupiter.api.*;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import javax.enterprise.inject.Any;
import javax.inject.Inject;

@QuarkusTest
@TestSecurity(user = "userOidc", roles = "viewer")
@OidcSecurity(claims = {
        @Claim(key = "email", value = "user@gmail.com"),
        @Claim(key = "sub", value = "c077e77a-7d45-11ec-90d6-0242ac120003")
})
public class BookletRegistryServiceTest {

    @Inject
    BookletRegistryService bookletRegistryService;

    @InjectMock
    RegistryRepository registryRepository;

    @Inject @Any
    InMemoryConnector connector;

    @BeforeAll
    public static void switchMyChannels() {
        InMemoryConnector.switchOutgoingChannelsToInMemory("document");
    }

    @AfterAll
    public static void revertMyChannels() {
        InMemoryConnector.clear();
    }

    @BeforeEach
    public void setUp(){
        Mockito.doNothing()
                .when(this.registryRepository)
                .persist(ArgumentMatchers.any(Registry.class));
    }

//    @Test
    public void testRegisterFirstTime(){
        Mockito.when(this.registryRepository.findLatest(Mockito.any(COMPANY_TYPE.class),
                Mockito.any(DOCUMENT_TYPE.class),Mockito.any(DOCUMENT_UNIT.class)))
                .thenReturn(null);
        var registry = this.bookletRegistryService.register(BookletRegisterEvent.builder()
                .bookletCount(5)
                .companyType(COMPANY_TYPE.COSMOS)
                .documentType(DOCUMENT_TYPE.LPA)
                .sequencePcs(50L)
                .build());

        var document = registry.getDocument();

        Assertions.assertNotNull(registry);
        Assertions.assertNotNull(document);
        Assertions.assertEquals(document.getStartSequence(),1L);
        Assertions.assertEquals(document.getEndSequence(),(5*50));
    }

//    @Test
    public void testRegisterAnother(){
        var dbData = Registry.builder()
                .document(Document.builder()
                        .startSequence(1L)
                        .endSequence(100L)
                        .build())
                .build();
        Mockito.when(this.registryRepository.findLatest(Mockito.any(COMPANY_TYPE.class),
                Mockito.any(DOCUMENT_TYPE.class),Mockito.any(DOCUMENT_UNIT.class)))
                .thenReturn(dbData);

        var bookletRegisterEvent = BookletRegisterEvent.builder()
                .bookletCount(2)
                .companyType(COMPANY_TYPE.COSMOS)
                .documentType(DOCUMENT_TYPE.LPA)
                .sequencePcs(50L)
                .build();
        var registry = this.bookletRegistryService.register(bookletRegisterEvent);

        var document = registry.getDocument();
        var endSequence = dbData.getDocument()
                .getEndSequence();

        Assertions.assertNotNull(registry);
        Assertions.assertNotNull(document);
        Assertions.assertEquals(document.getStartSequence(),endSequence + 1);
        Assertions.assertEquals(document.getEndSequence(), endSequence + ( bookletRegisterEvent.getBookletCount() * bookletRegisterEvent.getSequencePcs()));
    }

}
