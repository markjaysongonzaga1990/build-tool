package ent.cg.cmd.service.registry;

import ent.cg.boundary.Document;
import ent.cg.boundary.Registry;
import ent.cg.boundary.repository.RegistryRepository;
import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.core.enums.DOCUMENT_TYPE;
import ent.cg.core.enums.DOCUMENT_UNIT;
import ent.cg.core.event.SequenceRegisterEvent;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.OidcSecurity;
import io.smallrye.reactive.messaging.providers.connectors.InMemoryConnector;
import org.junit.jupiter.api.*;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import javax.enterprise.inject.Any;
import javax.inject.Inject;
import java.util.Arrays;

@QuarkusTest
@TestSecurity(user = "userOidc", roles = "viewer")
@OidcSecurity(claims = {
        @Claim(key = "email", value = "user@gmail.com"),
        @Claim(key = "sub", value = "c077e77a-7d45-11ec-90d6-0242ac120003")
})
public class SequenceRegistryServiceTest {


    @Inject
    SequenceRegistryService sequenceRegistryService;

    @InjectMock
    RegistryRepository registryRepository;

    @Inject @Any
    InMemoryConnector connector;

    @BeforeAll
    public static void switchMyChannels() {
        InMemoryConnector.switchOutgoingChannelsToInMemory("document");
    }

    @AfterAll
    public static void revertMyChannels() {
        InMemoryConnector.clear();
    }


    @BeforeEach
    public void setUp(){
        Mockito.doNothing()
                .when(this.registryRepository)
                .persist(ArgumentMatchers.any(Registry.class));
    }

    @Test
    public void registerNoOverlap(){
        var sequenceRegisterEvent = SequenceRegisterEvent.builder()
                .companyType(COMPANY_TYPE.GOLDEN_FUTURE)
                .sequenceFrom("000031")
                .sequenceTo("000050")
                .documentType(DOCUMENT_TYPE.LPA)
                .build();
        var registry = this.sequenceRegistryService.register(sequenceRegisterEvent);

        var document = registry.getDocument();

        Mockito.verify(this.registryRepository,Mockito.times(1))
                .findByUnitAndType(Mockito.any(COMPANY_TYPE.class),
                Mockito.any(DOCUMENT_TYPE.class),Mockito.any(DOCUMENT_UNIT.class));

        Assertions.assertNotNull(registry);
        Assertions.assertNotNull(document);
        Assertions.assertEquals(Long.parseLong(sequenceRegisterEvent.getSequenceFrom()),document.getStartSequence());
        Assertions.assertEquals(Long.parseLong(sequenceRegisterEvent.getSequenceTo()),document.getEndSequence());
    }

    @Test
    public void registerHasOverlap(){
        Mockito.when(this.registryRepository.findByUnitAndType(Mockito.any(COMPANY_TYPE.class),
                Mockito.any(DOCUMENT_TYPE.class),Mockito.any(DOCUMENT_UNIT.class)))
                .thenReturn(Arrays.asList(Registry.builder()
                        .document(Document.builder()
                                .startSequence(21L)
                                .endSequence(50L)
                                .build())
                        .build(),Registry.builder()
                        .document(Document.builder()
                                .startSequence(1L)
                                .endSequence(20L)
                                .build())
                        .build()));

        var sequenceRegisterEvent = SequenceRegisterEvent.builder()
                .companyType(COMPANY_TYPE.GOLDEN_FUTURE)
                .sequenceFrom("000021")
                .sequenceTo("000100")
                .documentType(DOCUMENT_TYPE.LPA)
                .build();

        var exception = Assertions.assertThrows(RuntimeException.class,() -> {
            var registry = this.sequenceRegistryService.register(sequenceRegisterEvent);
        });

        Mockito.verify(this.registryRepository,Mockito.times(1))
                .findByUnitAndType(Mockito.any(COMPANY_TYPE.class),
                        Mockito.any(DOCUMENT_TYPE.class),Mockito.any(DOCUMENT_UNIT.class));

        Assertions.assertEquals(exception.getMessage(),"Sequence Overlapped");
    }
}