package ent.cg.cmd.service.release;

import ent.cg.boundary.Registry;
import ent.cg.boundary.Release;
import ent.cg.boundary.repository.RegistryRepository;
import ent.cg.boundary.repository.ReleaseRepository;
import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.core.enums.DOCUMENT_TYPE;
import ent.cg.core.enums.DOCUMENT_UNIT;
import ent.cg.core.event.SequenceReleaseEvent;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.OidcSecurity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

@QuarkusTest
@TestSecurity(user = "userOidc", roles = "viewer")
@OidcSecurity(claims = {
        @Claim(key = "email", value = "user@gmail.com"),
        @Claim(key = "sub", value = "c077e77a-7d45-11ec-90d6-0242ac120003")
})
class SequenceReleaseServiceTest {

    @Inject
    SequenceReleaseService sequenceReleaseService;

    @InjectMock
    RegistryRepository registryRepository;

    @InjectMock
    ReleaseRepository releaseRepository;

    private Registry registry(Long id,Long from,Long to) {
        return Registry.builder()
                .id(id)
                .documentUnit(DOCUMENT_UNIT.SEQUENCE)
                .sequenceFrom(from)
                .sequenceTo(to)
                .build();
    }

    @BeforeEach
    public void setUp(){
        Mockito.doNothing()
                .when(this.releaseRepository)
                .persist(ArgumentMatchers.any(Release.class));
    }

    @Test
    public void testReleaseWithNoPreviousRelease(){
        var sequenceReleaseEvent = SequenceReleaseEvent.builder()
                .companyType(COMPANY_TYPE.GOLDEN_FUTURE)
                .documentUnit(DOCUMENT_UNIT.SEQUENCE)
                .documentType(DOCUMENT_TYPE.LPA)
                .sequenceFrom("000001")
                .sequenceTo("000050")
                .assignedPersonnelId(1L)
                .branchOfficeId(100L)
                .build();

        var registryList = Arrays.asList(registry(10L,1L,50L),
                                                      registry(1L,51L,100L),
                                                      registry(15L,200L,250L));

        Mockito.when(this.registryRepository.findAllRegistryStartingFromId(0L,sequenceReleaseEvent.getCompanyType(),sequenceReleaseEvent.getDocumentType()))
                .thenReturn(registryList);

        this.sequenceReleaseService.release(sequenceReleaseEvent);
    }

}