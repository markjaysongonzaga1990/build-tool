package ent.cg.cmd.service.release;

import ent.cg.boundary.Assignment;
import ent.cg.boundary.Registry;
import ent.cg.boundary.Release;
import ent.cg.boundary.repository.RegistryRepository;
import ent.cg.boundary.repository.ReleaseRepository;
import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.core.enums.DOCUMENT_TYPE;
import ent.cg.core.enums.DOCUMENT_UNIT;
import ent.cg.core.event.BookletReleaseEvent;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.OidcSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Comparator;

@QuarkusTest
@TestSecurity(user = "userOidc", roles = "viewer")
@OidcSecurity(claims = {
        @Claim(key = "email", value = "user@gmail.com"),
        @Claim(key = "sub", value = "c077e77a-7d45-11ec-90d6-0242ac120003")
})
class BookletReleaseServiceTest {

    @Inject
    BookletReleaseService bookletReleaseService;

    @InjectMock
    RegistryRepository registryRepository;

    @InjectMock
    ReleaseRepository releaseRepository;

    private BookletReleaseEvent getBookletReleaseEvent(int bookletcount) {
        var bookletReleaseEvent = BookletReleaseEvent.builder()
                .companyType(COMPANY_TYPE.COSMOS)
                .documentType(DOCUMENT_TYPE.LPA)
                .bookletCount(bookletcount)
                .assignedPersonnelId(1L)
                .branchOfficeId(1L)
                .build();
        return bookletReleaseEvent;
    }

    @BeforeEach
    public void setUp(){
        Mockito.doNothing()
                .when(this.releaseRepository)
                .persist(ArgumentMatchers.any(Release.class));
    }

    @Test
    void moveToNextRegistryButInsufficient(){
        var bookletReleaseEvent = getBookletReleaseEvent(10);

        Mockito.when(this.releaseRepository.findAllReleases(Mockito.any(DOCUMENT_UNIT.class),
                Mockito.any(DOCUMENT_TYPE.class),
                Mockito.any(COMPANY_TYPE.class)))
                .thenReturn(Arrays.asList(Release.builder()
                                .id(1L)
                                .registry(Registry.builder().id(51L).build())
                                .build(),Release.builder()
                                .id(5L)
                                .registry(Registry.builder().id(1L).build())
                                .build(),
                        Release.builder()
                                .id(2L)
                                .registry(Registry.builder().id(31L).build())
                                .build()));

        var registryToVerify = Registry.builder()
                .id(2L)
                .documentUnit(DOCUMENT_UNIT.BOOKLET)
                .sequencePcs(50L)
                .input(5)
                .build();
        Mockito.when(this.registryRepository.findAllRegistryStartingFromId(Mockito.anyLong(),
                Mockito.any(COMPANY_TYPE.class),
                Mockito.any(DOCUMENT_TYPE.class)))
                .thenReturn(Arrays.asList(Registry.builder()
                                .id(1L)
                                .documentUnit(DOCUMENT_UNIT.BOOKLET)
                                .sequencePcs(50L)
                                .input(8)
                                .build(),
                        registryToVerify));

        Mockito.when(this.releaseRepository.findAllReleaseByRegistryId(Mockito.anyLong()))
                .thenReturn(Arrays.asList(Release.builder()
                                .input(6)
                                .build(),
                        Release.builder()
                                .input(2)
                                .build()));

        var ex = Assertions.assertThrows(RuntimeException.class,() -> {
            this.bookletReleaseService.release(bookletReleaseEvent);
        });

        Assertions.assertEquals("The maximum booklet that you can release in this transaction is 5.Registry id : 2 has 0 out of 5 issued.",ex.getMessage());

        Mockito.verify(releaseRepository,Mockito.times(1))
                .findAllReleases(Mockito.any(DOCUMENT_UNIT.class),
                        Mockito.any(DOCUMENT_TYPE.class),
                        Mockito.any(COMPANY_TYPE.class));

        Mockito.verify(registryRepository,Mockito.times(1))
                .findAllRegistryStartingFromId(Mockito.anyLong(),
                        Mockito.any(COMPANY_TYPE.class),
                        Mockito.any(DOCUMENT_TYPE.class));

        Mockito.verify(releaseRepository,Mockito.times(1))
                .findAllReleaseByRegistryId(Mockito.anyLong());
    }

    @Test
    void moveToNextRegistry(){
        var bookletReleaseEvent = getBookletReleaseEvent(3);

        Mockito.when(this.releaseRepository.findAllReleases(Mockito.any(DOCUMENT_UNIT.class),
                Mockito.any(DOCUMENT_TYPE.class),
                Mockito.any(COMPANY_TYPE.class)))
                .thenReturn(Arrays.asList(Release.builder()
                                .id(1L)
                                .registry(Registry.builder().id(51L).build())
                                .build(),Release.builder()
                                .id(5L)
                                .registry(Registry.builder().id(1L).build())
                                .build(),
                        Release.builder()
                                .id(2L)
                                .registry(Registry.builder().id(31L).build())
                                .build()));

        //only 1 registry record
        var registryToVerify = Registry.builder()
                .id(2L)
                .documentUnit(DOCUMENT_UNIT.BOOKLET)
                .sequencePcs(50L)
                .input(5)
                .build();
        Mockito.when(this.registryRepository.findAllRegistryStartingFromId(Mockito.anyLong(),
                Mockito.any(COMPANY_TYPE.class),
                Mockito.any(DOCUMENT_TYPE.class)))
                .thenReturn(Arrays.asList(Registry.builder()
                        .id(1L)
                        .documentUnit(DOCUMENT_UNIT.BOOKLET)
                        .sequencePcs(50L)
                        .input(8)
                        .build(),
                        registryToVerify));

        Mockito.when(this.releaseRepository.findAllReleaseByRegistryId(Mockito.anyLong()))
                .thenReturn(Arrays.asList(Release.builder()
                                .input(6)
                                .build(),
                        Release.builder()
                                .input(2)
                                .build()));

        var release = this.bookletReleaseService.release(bookletReleaseEvent);
        var registry = release.getRegistry();
        var assignments = release.getAssignments();

        Assertions.assertNotNull(release);
        Assertions.assertEquals(registryToVerify.getId(),registry.getId());
        Assertions.assertEquals(assignments.size(),registryToVerify.getSequencePcs() * bookletReleaseEvent.getBookletCount());

        Assertions.assertEquals(assignments.stream().sorted(Comparator.comparingLong(Assignment::getSequenceNumber)).findFirst().get().getSequenceNumber(),1L);
        Assertions.assertEquals(assignments.stream().sorted(Comparator.comparingLong(Assignment::getSequenceNumber).reversed()).findFirst().get().getSequenceNumber(),150);

        Mockito.verify(releaseRepository,Mockito.times(1))
                .findAllReleases(Mockito.any(DOCUMENT_UNIT.class),
                        Mockito.any(DOCUMENT_TYPE.class),
                        Mockito.any(COMPANY_TYPE.class));

        Mockito.verify(registryRepository,Mockito.times(1))
                .findAllRegistryStartingFromId(Mockito.anyLong(),
                        Mockito.any(COMPANY_TYPE.class),
                        Mockito.any(DOCUMENT_TYPE.class));

        Mockito.verify(releaseRepository,Mockito.times(1))
                .findAllReleaseByRegistryId(Mockito.anyLong());
    }

    @Test
    void insufficient(){
        var bookletReleaseEvent = getBookletReleaseEvent(7);

        Mockito.when(this.releaseRepository.findAllReleases(Mockito.any(DOCUMENT_UNIT.class),
                Mockito.any(DOCUMENT_TYPE.class),
                Mockito.any(COMPANY_TYPE.class)))
                .thenReturn(Arrays.asList(Release.builder()
                                .id(1L)
                                .registry(Registry.builder().id(51L).build())
                                .build(),Release.builder()
                                .id(5L)
                                .registry(Registry.builder().id(1L).build())
                                .build(),
                        Release.builder()
                                .id(2L)
                                .registry(Registry.builder().id(31L).build())
                                .build()));

        //only 1 registry record
        Mockito.when(this.registryRepository.findAllRegistryStartingFromId(Mockito.anyLong(),
                Mockito.any(COMPANY_TYPE.class),
                Mockito.any(DOCUMENT_TYPE.class)))
                .thenReturn(Arrays.asList(Registry.builder()
                        .id(1L)
                        .documentUnit(DOCUMENT_UNIT.BOOKLET)
                        .sequencePcs(50L)
                        .input(6)
                        .build()));

        Mockito.when(this.releaseRepository.findAllReleaseByRegistryId(Mockito.anyLong()))
                .thenReturn(Arrays.asList(Release.builder()
                                .input(3)
                                .build(),
                        Release.builder()
                                .input(2)
                                .build()));

        var ex = Assertions.assertThrows(RuntimeException.class,() -> {
            this.bookletReleaseService.release(bookletReleaseEvent);
        });

        Assertions.assertEquals(ex.getMessage(), "The maximum booklet that you can release in this transaction is 1.Registry id : 1 has 5 out of 6 issued.");

        Mockito.verify(releaseRepository,Mockito.times(1))
                .findAllReleases(Mockito.any(DOCUMENT_UNIT.class),
                        Mockito.any(DOCUMENT_TYPE.class),
                        Mockito.any(COMPANY_TYPE.class));

        Mockito.verify(registryRepository,Mockito.times(1))
                .findAllRegistryStartingFromId(Mockito.anyLong(),
                        Mockito.any(COMPANY_TYPE.class),
                        Mockito.any(DOCUMENT_TYPE.class));

        Mockito.verify(releaseRepository,Mockito.times(1))
                .findAllReleaseByRegistryId(Mockito.anyLong());
    }

    @Test
    void noAvailDocsToRelease(){
        var bookletReleaseEvent = getBookletReleaseEvent(3);

        Mockito.when(this.releaseRepository.findAllReleases(Mockito.any(DOCUMENT_UNIT.class),
                Mockito.any(DOCUMENT_TYPE.class),
                Mockito.any(COMPANY_TYPE.class)))
        .thenReturn(Arrays.asList(Release.builder()
                        .id(1L)
                        .registry(Registry.builder().id(51L).build())
                        .build(),Release.builder()
                .id(5L)
                .registry(Registry.builder().id(1L).build())
                .build(),
                Release.builder()
                        .id(2L)
                        .registry(Registry.builder().id(31L).build())
                        .build()));

        //only 1 registry record
        Mockito.when(this.registryRepository.findAllRegistryStartingFromId(Mockito.anyLong(),
                Mockito.any(COMPANY_TYPE.class),
                Mockito.any(DOCUMENT_TYPE.class)))
                .thenReturn(Arrays.asList(Registry.builder()
                        .id(1L)
                        .documentUnit(DOCUMENT_UNIT.BOOKLET)
                        .sequencePcs(50L)
                        .input(5)
                        .build()));

        Mockito.when(this.releaseRepository.findAllReleaseByRegistryId(Mockito.anyLong()))
                .thenReturn(Arrays.asList(Release.builder()
                        .input(3)
                        .build(),
                        Release.builder()
                        .input(2)
                        .build()));

        var ex = Assertions.assertThrows(RuntimeException.class,() -> {
            this.bookletReleaseService.release(bookletReleaseEvent);
        });

        Assertions.assertEquals(ex.getMessage(),"No Available Document to release. All booklets have been released already.");

        Mockito.verify(releaseRepository,Mockito.times(1))
                .findAllReleases(Mockito.any(DOCUMENT_UNIT.class),
                        Mockito.any(DOCUMENT_TYPE.class),
                        Mockito.any(COMPANY_TYPE.class));

        Mockito.verify(registryRepository,Mockito.times(1))
                .findAllRegistryStartingFromId(Mockito.anyLong(),
                        Mockito.any(COMPANY_TYPE.class),
                        Mockito.any(DOCUMENT_TYPE.class));

        Mockito.verify(releaseRepository,Mockito.times(1))
                .findAllReleaseByRegistryId(Mockito.anyLong());
    }

    @Test
    void noRegistryRecord(){
        var bookletReleaseEvent = getBookletReleaseEvent(3);
        Mockito.when(this.registryRepository.findAllRegistryStartingFromId(Mockito.anyLong(),
                Mockito.any(COMPANY_TYPE.class),
                Mockito.any(DOCUMENT_TYPE.class)))
                .thenReturn(Arrays.asList());

        var ex = Assertions.assertThrows(RuntimeException.class,() -> {
            this.bookletReleaseService.release(bookletReleaseEvent);
        });

        Assertions.assertEquals(ex.getMessage(),"""
                No Registry record found! Cannot proceed to releasing.
                Make sure that there is an available document to release first.
                """);

        Mockito.verify(releaseRepository,Mockito.times(1))
                .findAllReleases(Mockito.any(DOCUMENT_UNIT.class),
                        Mockito.any(DOCUMENT_TYPE.class),
                        Mockito.any(COMPANY_TYPE.class));

        Mockito.verify(registryRepository,Mockito.times(1))
                .findAllRegistryStartingFromId(Mockito.anyLong(),
                        Mockito.any(COMPANY_TYPE.class),
                        Mockito.any(DOCUMENT_TYPE.class));

        Mockito.verify(releaseRepository,Mockito.times(0))
                .findAllReleaseByRegistryId(Mockito.anyLong());
    }

    @Test
    void releaseFirstTime() {
        BookletReleaseEvent bookletReleaseEvent = getBookletReleaseEvent(3);

        //no release record yet
        Mockito.when(this.releaseRepository.findAllReleases(Mockito.any(DOCUMENT_UNIT.class),
                Mockito.any(DOCUMENT_TYPE.class),
                Mockito.any(COMPANY_TYPE.class))).thenReturn(Arrays.asList());

        var registry = Registry.builder()
                .id(1L)
                .documentUnit(DOCUMENT_UNIT.BOOKLET)
                .sequencePcs(50L)
                .input(5)
                .build();
        Mockito.when(this.registryRepository.findAllRegistryStartingFromId(Mockito.anyLong(),
                Mockito.any(COMPANY_TYPE.class),
                Mockito.any(DOCUMENT_TYPE.class)))
        .thenReturn(Arrays.asList(Registry.builder()
                .id(2L)
                .documentUnit(DOCUMENT_UNIT.BOOKLET)
                .sequencePcs(50L)
                .input(3)
                .build(),registry));


        var release = this.bookletReleaseService.release(bookletReleaseEvent);
        var assignments = release.getAssignments();

        Assertions.assertNotNull(release);
        Assertions.assertEquals(registry.getId(),release.getRegistry().getId());
        Assertions.assertNotNull(assignments);
        Assertions.assertEquals(assignments.size(),registry.getSequencePcs() * bookletReleaseEvent.getBookletCount());
    }


}