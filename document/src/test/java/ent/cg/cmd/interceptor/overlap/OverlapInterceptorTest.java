package ent.cg.cmd.interceptor.overlap;

import ent.cg.boundary.Document;
import ent.cg.boundary.Registry;
import ent.cg.boundary.repository.RegistryRepository;
import ent.cg.cmd.service.registry.SequenceRegistryService;
import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.core.enums.DOCUMENT_TYPE;
import ent.cg.core.enums.DOCUMENT_UNIT;
import ent.cg.core.event.SequenceRegisterEvent;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.OidcSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import javax.inject.Inject;
import java.util.Arrays;

@QuarkusTest
@TestSecurity(user = "userOidc", roles = "viewer")
@OidcSecurity(claims = {
        @Claim(key = "email", value = "user@gmail.com"),
        @Claim(key = "sub", value = "c077e77a-7d45-11ec-90d6-0242ac120003")
})
class OverlapInterceptorTest {

    @Inject
    SequenceRegistryService sequenceRegistryService;

    @InjectMock
    RegistryRepository registryRepository;

    @BeforeEach
    public void setUp(){
        Mockito.doNothing()
                .when(this.registryRepository)
                .persist(ArgumentMatchers.any(Registry.class));
    }

    @Test
    public void testNoOverlapMultipleDocs() {
        Mockito.when(this.registryRepository.findByUnitAndType(Mockito.any(COMPANY_TYPE.class),
                Mockito.any(DOCUMENT_TYPE.class),Mockito.any(DOCUMENT_UNIT.class)))
                .thenReturn(Arrays.asList(Registry.builder()
                        .document(Document.builder()
                                .startSequence(1L)
                                .endSequence(50L)
                                .build())
                        .build(),Registry.builder()
                        .document(Document.builder()
                                .startSequence(60L)
                                .endSequence(80L)
                                .build())
                        .build(),Registry.builder()
                        .document(Document.builder()
                                .startSequence(160L)
                                .endSequence(250L)
                                .build())
                        .build()));

        var sequenceRegisterEvent = SequenceRegisterEvent.builder()
                .companyType(COMPANY_TYPE.GOLDEN_FUTURE)
                .documentType(DOCUMENT_TYPE.LPA)
                .sequenceFrom("000100")
                .sequenceTo("000150")
                .build();
        var registry = this.sequenceRegistryService.register(sequenceRegisterEvent);

        Mockito.verify(this.registryRepository,Mockito.times(1))
                .findByUnitAndType(Mockito.any(COMPANY_TYPE.class),
                        Mockito.any(DOCUMENT_TYPE.class),Mockito.any(DOCUMENT_UNIT.class));

        var document = registry.getDocument();
        Assertions.assertNotNull(registry);
        Assertions.assertNotNull(document);
        Assertions.assertEquals(Long.parseLong(sequenceRegisterEvent.getSequenceFrom()),document.getStartSequence());
        Assertions.assertEquals(Long.parseLong(sequenceRegisterEvent.getSequenceTo()),document.getEndSequence());
    }

    @Test
    public void testOverlap() {
        Mockito.when(this.registryRepository.findByUnitAndType(Mockito.any(COMPANY_TYPE.class),
                Mockito.any(DOCUMENT_TYPE.class),Mockito.any(DOCUMENT_UNIT.class)))
                .thenReturn(Arrays.asList(Registry.builder()
                        .document(Document.builder()
                                .startSequence(1L)
                                .endSequence(100L)
                                .build())
                        .build()));

        var exception = Assertions.assertThrows(RuntimeException.class,() -> {
            this.sequenceRegistryService.register(SequenceRegisterEvent.builder()
                    .companyType(COMPANY_TYPE.GOLDEN_FUTURE)
                    .documentType(DOCUMENT_TYPE.LPA)
                    .sequenceFrom("000100")
                    .sequenceTo("000200")
                    .build());
        });

        Assertions.assertEquals("Sequence Overlapped",exception.getMessage());
        Mockito.verify(this.registryRepository,Mockito.times(1))
                .findByUnitAndType(Mockito.any(COMPANY_TYPE.class),
                        Mockito.any(DOCUMENT_TYPE.class),Mockito.any(DOCUMENT_UNIT.class));
    }

    @Test
    public void testOverlapInsideRange() {
        Mockito.when(this.registryRepository.findByUnitAndType(Mockito.any(COMPANY_TYPE.class),
                Mockito.any(DOCUMENT_TYPE.class),Mockito.any(DOCUMENT_UNIT.class)))
                .thenReturn(Arrays.asList(Registry.builder()
                        .document(Document.builder()
                                .startSequence(150L)
                                .endSequence(190L)
                                .build())
                        .build()));

        var exception = Assertions.assertThrows(RuntimeException.class,() -> {
            this.sequenceRegistryService.register(SequenceRegisterEvent.builder()
                    .companyType(COMPANY_TYPE.GOLDEN_FUTURE)
                    .documentType(DOCUMENT_TYPE.LPA)
                    .sequenceFrom("000100")
                    .sequenceTo("000200")
                    .build());
        });

        Assertions.assertEquals("Sequence Overlapped",exception.getMessage());
        Mockito.verify(this.registryRepository,Mockito.times(1))
                .findByUnitAndType(Mockito.any(COMPANY_TYPE.class),
                        Mockito.any(DOCUMENT_TYPE.class),Mockito.any(DOCUMENT_UNIT.class));
    }

}