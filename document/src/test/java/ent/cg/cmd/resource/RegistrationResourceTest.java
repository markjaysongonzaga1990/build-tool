package ent.cg.cmd.resource;

import ent.cg.core.enums.DOCUMENT_TYPE;
import ent.cg.core.event.BookletRegisterEvent;
import ent.cg.core.event.SequenceRegisterEvent;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.OidcSecurity;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

@QuarkusTest
@TestSecurity(user = "userOidc", roles = "viewer")
@OidcSecurity(claims = {
        @Claim(key = "email", value = "user@gmail.com"),
        @Claim(key = "Subject", value = "c077e77a-7d45-11ec-90d6-0242ac120003")
})
public class RegistrationResourceTest {

//    @Test
    public void testBookletRegistration(){
        given()
                .body(BookletRegisterEvent.builder()
                        .bookletCount(3)
                        .documentType(DOCUMENT_TYPE.LPA)
                        .build())
                .contentType(ContentType.JSON)
                .when().post("/registration/booklet")
                .then()
                    .statusCode(202);
    }

//    @Test
    public void testBookletRegistrationInvalid(){
        given()
                .body(BookletRegisterEvent.builder()
                        .bookletCount(-1)
                        .documentType(DOCUMENT_TYPE.LPA)
                        .build())
                .contentType(ContentType.JSON)
                .when().post("/registration/booklet")
                .then()
                .statusCode(400);
    }

//    @Test
    public void testSequenceRegistration(){
        given()
                .body(SequenceRegisterEvent.builder()
                        .sequenceFrom("000001")
                        .sequenceTo("000050")
                        .documentType(DOCUMENT_TYPE.LPA)
                        .build())
                .contentType(ContentType.JSON)
                .when().post("/registration/sequence")
                .then()
                .statusCode(202);
    }

//    @Test
    public void testSequenceInvalid(){
        given()
                .body(SequenceRegisterEvent.builder()
                        .sequenceFrom("00s101")
                        .sequenceTo("000050")
                        .documentType(DOCUMENT_TYPE.LPA)
                        .build())
                .contentType(ContentType.JSON)
                .when().post("/registration/sequence")
                .then()
                .statusCode(400);
    }

}
