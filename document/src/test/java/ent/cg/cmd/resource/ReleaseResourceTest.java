package ent.cg.cmd.resource;

import ent.cg.core.enums.DOCUMENT_TYPE;
import ent.cg.core.event.BookletRegisterEvent;
import ent.cg.core.event.BookletReleaseEvent;
import ent.cg.core.event.SequenceRegisterEvent;
import ent.cg.core.event.SequenceReleaseEvent;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.OidcSecurity;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

@QuarkusTest
@TestSecurity(user = "userOidc", roles = "viewer")
@OidcSecurity(claims = {
        @Claim(key = "email", value = "user@gmail.com"),
        @Claim(key = "sub", value = "c077e77a-7d45-11ec-90d6-0242ac120003")
})
public class ReleaseResourceTest {

    @Test
    public void testBookletRelease(){
        given()
                .body(BookletReleaseEvent.builder()
                        .bookletCount(3)
                        .documentType(DOCUMENT_TYPE.LPA)
                        .branchOfficeId(0L)
                        .assignedPersonnelId(0L)
                        .build())
                .contentType(ContentType.JSON)
                .when().post("/release/booklet")
                .then()
                    .statusCode(202);
    }

    @Test
    public void testBookletReleaseInvalid(){
        given()
                .body(BookletRegisterEvent.builder()
                        .bookletCount(-1)
                        .documentType(DOCUMENT_TYPE.LPA)
                        .build())
                .contentType(ContentType.JSON)
                .when().post("/release/booklet")
                .then()
                .statusCode(400);
    }

    @Test
    public void testSequenceRelease(){
        given()
                .body(SequenceReleaseEvent.builder()
                        .sequenceFrom("000001")
                        .sequenceTo("000050")
                        .branchOfficeId(1L)
                        .assignedPersonnelId(0L)
                        .documentType(DOCUMENT_TYPE.LPA)
                        .build())
                .contentType(ContentType.JSON)
                .when().post("/release/sequence")
                .then()
                .statusCode(202);
    }

    @Test
    public void testSequenceInvalid(){
        given()
                .body(SequenceRegisterEvent.builder()
                        .sequenceFrom("00s101")
                        .sequenceTo("000050")
                        .documentType(DOCUMENT_TYPE.LPA)
                        .build())
                .contentType(ContentType.JSON)
                .when().post("/release/sequence")
                .then()
                .statusCode(400);
    }

}
