package ent.cg.qry.resource;

import ent.cg.core.enums.DOCUMENT_TYPE;
import ent.cg.qry.service.ReleaseQueryService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/query/release")
public class ReleaseQueryResource {

    @Inject
    ReleaseQueryService releaseQueryService;

    @GET
    @Path("/{id}")
    public Response findById(@PathParam("id") Long id){
        var releaseRecord = this.releaseQueryService.findById(id);
        return Response.ok(releaseRecord).build();
    }

    @GET
    @Path("/booklet/{type}")
    public Response findAllBookletRegistration(@DefaultValue("20") @QueryParam ("size")Integer size,
                                               @DefaultValue ("0")@QueryParam("page") Integer page,
                                               @PathParam("type")DOCUMENT_TYPE documentType){
        var registryBookletQueryProjections = this.releaseQueryService.fetchAllReleasedBooklet(documentType, size, page);
        return Response.ok(registryBookletQueryProjections).build();
    }

    @GET
    @Path("/sequence/{type}")
    public Response findAllSequenceRegistration(@DefaultValue("20") @QueryParam ("size")Integer size,
                                               @DefaultValue ("0")@QueryParam("page") Integer page,
                                               @PathParam("type") DOCUMENT_TYPE documentType){
        var registryBookletQueryProjections = this.releaseQueryService.fetchAllReleasedSequence(documentType, size, page);
        return Response.ok(registryBookletQueryProjections).build();
    }
}
