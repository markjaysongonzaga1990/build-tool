package ent.cg.qry.resource;

import ent.cg.core.enums.DOCUMENT_TYPE;
import ent.cg.qry.service.RegistryQueryService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/query/registration")
public class RegistryQueryResource {

    @Inject
    RegistryQueryService registryQueryService;

    @GET
    @Path("/booklet/{type}")
    public Response findAllBookletRegistration(@DefaultValue("20") @QueryParam ("size")Integer size,
                                               @DefaultValue ("0")@QueryParam("page") Integer page,
                                               @PathParam("type") DOCUMENT_TYPE documentType){
        var registryBookletQueryProjections = this.registryQueryService.fetchAllBooklet(documentType, size, page);
        return Response.ok(registryBookletQueryProjections).build();
    }

    @GET
    @Path("/sequence/{type}")
    public Response findAllSequenceRegistration(@DefaultValue("20") @QueryParam ("size")Integer size,
                                               @DefaultValue ("0")@QueryParam("page") Integer page,
                                               @PathParam("type") DOCUMENT_TYPE documentType){
        var registryBookletQueryProjections = this.registryQueryService.fetchAllSequence(documentType, size, page);
        return Response.ok(registryBookletQueryProjections).build();
    }
}
