package ent.cg.qry.service;

import ent.cg.boundary.repository.RegistryRepository;
import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.core.enums.DOCUMENT_TYPE;
import ent.cg.core.keycloak.service.KeycloakUserService;
import ent.cg.qry.projection.RegistryBookletQueryProjection;
import ent.cg.qry.projection.RegistrySequenceQueryProjection;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ApplicationScoped
public class RegistryQueryService {

    @Inject
    KeycloakUserService keycloakUserService;

    @Inject
    RegistryRepository registryRepository;

    public List<RegistryBookletQueryProjection> fetchAllBooklet(DOCUMENT_TYPE documentType, Integer size, Integer page) {
        var company = COMPANY_TYPE.COSMOS;
        return this.registryRepository.findAllPageByCompanyAndType(company,documentType,size,page)
                .stream().map(x-> {
                    var document = x.getDocument();
                    var userName = this.keycloakUserService.retrieveName(x.getEncodedBy());
                    return RegistryBookletQueryProjection.builder()
                            .id(x.getId())
                            .sequencePcs(x.getSequencePcs())
                            .input(x.getInput())
                            .documentType(x.getDocumentType())
                            .endSequence(document.getEndSequence())
                            .startSequence(document.getStartSequence())
                            .encodedBy(userName)
                            .build();
                })
                .collect(Collectors.toList());
    }

    public List<RegistrySequenceQueryProjection> fetchAllSequence(DOCUMENT_TYPE documentType, Integer size, Integer page) {
        var company = COMPANY_TYPE.GOLDEN_FUTURE;
        return this.registryRepository.findAllPageByCompanyAndType(company,documentType,size,page)
                .stream()
                .map(x-> {
                    var userName = this.keycloakUserService.retrieveName(x.getEncodedBy());
                    return RegistrySequenceQueryProjection.builder()
                            .id(x.getId())
                            .documentType(x.getDocumentType())
                            .sequenceFrom(x.getSequenceFrom())
                            .sequenceTo(x.getSequenceTo())
                            .encodedBy(userName)
                            .build();
                })
                .collect(Collectors.toList());
    }
}
