package ent.cg.qry.service;

import ent.cg.boundary.repository.ReleaseRepository;
import ent.cg.core.dto.OfficeQuery;
import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.core.enums.DOCUMENT_TYPE;
import ent.cg.core.enums.DOCUMENT_UNIT;
import ent.cg.core.keycloak.service.KeycloakUserService;
import ent.cg.qry.projection.ReleaseAssignmentQueryProjection;
import ent.cg.qry.projection.ReleaseQueryProjection;
import io.quarkus.panache.common.Page;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.LockModeType;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ApplicationScoped
public class ReleaseQueryService {

    @Inject
    KeycloakUserService keycloakUserService;

    @Inject
    ReleaseRepository releaseRepository;

    public ReleaseQueryProjection findById(Long id){
        return this.releaseRepository.findByIdOptional(id, LockModeType.OPTIMISTIC)
                .map(x-> {
                    var list = x.getAssignments()
                            .parallelStream()
                            .map(y-> ReleaseAssignmentQueryProjection.builder()
                                    .id(y.getId())
                                    .sequenceNumber(y.getSequenceNumber())
                                    .officeQuery(OfficeQuery.builder()
                                            .id(y.getId())
                                            //TODO : fetch office details from office service
                                            .build())
                                    .build())
                            .collect(Collectors.toList());
                    var releaseQueryProjection = ReleaseQueryProjection.builder()
                            .createdDate(x.getCreatedDate())
                            .id(x.getId())
                            .documentType(x.getRegistry().getDocumentType())
                            .releasedBy(this.keycloakUserService.retrieveName(x.getReleasedBy()))
                            .endSequence(x.getSequenceTo())
                            .startSequence(x.getSequenceFrom())
                            .officesList(list)
                            .build();
                    return releaseQueryProjection;
                })
                .orElseThrow(() -> new RuntimeException("Release record not found!"));
    }

    public List<ReleaseQueryProjection> fetchAllReleasedBooklet(DOCUMENT_TYPE documentType, Integer size, Integer page) {
        var company = COMPANY_TYPE.COSMOS;
        var pageParam = Page.of(page, size);

        return this.releaseRepository.findAllReleasesPage(DOCUMENT_UNIT.BOOKLET,documentType,company, pageParam)
                .parallelStream()
                .map(x-> ReleaseQueryProjection.builder()
                        .createdDate(x.getCreatedDate())
                        .id(x.getId())
                        .documentType(x.getRegistry().getDocumentType())
                        .releasedBy(this.keycloakUserService.retrieveName(x.getReleasedBy()))
                        .endSequence(x.getSequenceTo())
                        .startSequence(x.getSequenceFrom())
                        .build())
                .collect(Collectors.toList());
    }

    public List<ReleaseQueryProjection> fetchAllReleasedSequence(DOCUMENT_TYPE documentType, Integer size, Integer page) {
        var company = COMPANY_TYPE.GOLDEN_FUTURE;
        var pageParam = Page.of(page, size);

        return this.releaseRepository.findAllReleasesPage(DOCUMENT_UNIT.SEQUENCE,documentType,company,pageParam)
                .parallelStream()
                .map(x-> ReleaseQueryProjection.builder()
                        .createdDate(x.getCreatedDate())
                        .id(x.getId())
                        .documentType(x.getRegistry().getDocumentType())
                        .releasedBy(this.keycloakUserService.retrieveName(x.getReleasedBy()))
                        .endSequence(x.getSequenceTo())
                        .startSequence(x.getSequenceFrom())
                        .build())
                .collect(Collectors.toList());
    }
}
