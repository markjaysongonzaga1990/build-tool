package ent.cg.qry.projection;

import ent.cg.core.dto.OfficeQuery;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RegisterForReflection
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReleaseAssignmentQueryProjection {
    Long id;
    Long sequenceNumber;
    OfficeQuery officeQuery;
}
