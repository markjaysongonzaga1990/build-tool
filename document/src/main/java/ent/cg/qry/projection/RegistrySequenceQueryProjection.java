package ent.cg.qry.projection;

import ent.cg.core.dto.UserQuery;
import ent.cg.core.enums.DOCUMENT_TYPE;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@RegisterForReflection
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegistrySequenceQueryProjection {
    Long id;
    DOCUMENT_TYPE documentType;

    Long sequenceFrom;
    Long sequenceTo;

    UserQuery encodedBy;
}
