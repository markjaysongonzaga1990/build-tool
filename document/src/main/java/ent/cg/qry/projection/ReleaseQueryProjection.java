package ent.cg.qry.projection;

import ent.cg.core.dto.OfficeQuery;
import ent.cg.core.dto.UserQuery;
import ent.cg.core.enums.DOCUMENT_TYPE;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RegisterForReflection
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReleaseQueryProjection {
    Long id;
    DOCUMENT_TYPE documentType;

    Long startSequence;
    Long endSequence;

    List<ReleaseAssignmentQueryProjection> officesList;

    UserQuery releasedBy;
    Date createdDate;
}
