package ent.cg.qry.projection;

import ent.cg.core.dto.UserQuery;
import ent.cg.core.enums.DOCUMENT_TYPE;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RegisterForReflection
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegistryBookletQueryProjection {
    Long id;
    DOCUMENT_TYPE documentType;
    Integer input;
    Long sequencePcs;

    Long startSequence;
    Long endSequence;

    UserQuery encodedBy;
}
