package ent.cg.cmd.interceptor.overlap;

import ent.cg.boundary.Registry;
import ent.cg.boundary.repository.RegistryRepository;
import ent.cg.core.enums.DOCUMENT_UNIT;
import ent.cg.core.event.SequenceRegisterEvent;
import io.quarkus.arc.Priority;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Slf4j
@Overlap
@Priority(1)
@Interceptor
public class OverlapInterceptor {

    @Inject
    RegistryRepository registryRepository;

    @AroundInvoke
    public Object aroundInvoke(InvocationContext context) throws Exception {
        var event = (SequenceRegisterEvent) context.getParameters()[0];

        log.info("{} :: {}",this.getClass().getSimpleName(),event.toString());

        var companyType = event.getCompanyType();
        var documentType = event.getDocumentType();
        var documentUnit = DOCUMENT_UNIT.SEQUENCE;

        var list = this.registryRepository.findByUnitAndType(companyType, documentType, documentUnit);

        var startRange = Long.parseLong(event.getSequenceFrom());
        var endRange = Long.parseLong(event.getSequenceTo());

        var hasOverlapped = list.parallelStream()
                .map(Registry::getDocument)
                .filter(document -> {
                    var aStart = document.getStartSequence();
                    var aEnd = document.getEndSequence();
                    var isContains = (aStart >= startRange && aStart <= endRange) ||
                            (aEnd >= startRange && aEnd <= endRange) ||
                            (startRange >= aStart && startRange <= aEnd) ||
                            (endRange >= aStart && endRange <= aEnd);

                    return isContains;
                }).findFirst().isPresent();

        if(hasOverlapped) throw new RuntimeException("Sequence Overlapped");

        return context.proceed();
    }

}
