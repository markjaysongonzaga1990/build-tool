package ent.cg.cmd.interceptor.message;

import com.fasterxml.jackson.databind.ObjectMapper;
import ent.cg.core.dto.BeanData;
import ent.cg.core.enums.TRANSACTION_TYPE;
import ent.cg.core.enums.TRANSACTION_RESULT;
import ent.cg.core.event.ApprovalEvent;
import ent.cg.core.event.BaseEvent;
import ent.cg.core.event.BookletRegisterEvent;
import ent.cg.core.event.BookletReleaseEvent;
import io.quarkus.arc.Priority;
import io.vertx.mutiny.core.eventbus.EventBus;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Slf4j
@Message
@Priority(150)
@Interceptor
public class MessageEventInterceptor {

    @Inject
    EventBus eventBus;

    @AroundInvoke
    public Object aroundInvoke(InvocationContext context) {
        log.info("{} ::",this.getClass().getSimpleName());
        var eventParam = (BaseEvent) context.getParameters()[0];

        Object ret = null;

        var type = TRANSACTION_RESULT.SUCCESS;
        var message = "";
        var event = eventParam.eventName();

        try {
            var mapper = new ObjectMapper();
            ret = context.proceed();
            message = mapper.writeValueAsString(ret);

        } catch (Exception e) {
            type = TRANSACTION_RESULT.ERROR;
            message = e.getMessage();
            throw e;
        }finally {
            this.eventBus.send("JMSEvent", BeanData.builder()
                    .transactionResult(type)
                    .user(eventParam.getUser())
                    .companyType(eventParam.getCompanyType())
                    .transactionType(findTransactionType(eventParam))
                    .event(event)
                    .message(message)
                    .build());

            return ret;
        }
    }

    private TRANSACTION_TYPE findTransactionType(BaseEvent eventParam) {
        if(eventParam instanceof BookletRegisterEvent){
            return TRANSACTION_TYPE.REGISTRATION;
        }
        if(eventParam instanceof BookletReleaseEvent){
            return TRANSACTION_TYPE.RELEASE;
        }
        if(eventParam instanceof ApprovalEvent){
            return TRANSACTION_TYPE.APPROVAL;
        }

        return TRANSACTION_TYPE.TRANSMITTAL;
    }

}
