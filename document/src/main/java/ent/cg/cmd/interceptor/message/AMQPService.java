package ent.cg.cmd.interceptor.message;

import ent.cg.core.dto.BeanData;
import io.quarkus.vertx.ConsumeEvent;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

import javax.enterprise.context.ApplicationScoped;

@Slf4j
@ApplicationScoped
public class AMQPService {

    @Channel("document-out")
    Emitter<BeanData> metaDataEmitter;

    @ConsumeEvent("JMSEvent")
    public void sendMessage(BeanData beanData){
        var message = beanData.getMessage();
        var type = beanData.getTransactionResult();
        var metaDataEvent = beanData.getEvent();

        log.info("""
                Sending :: {}
                Type :: {}
                Event :: {}
                """, message, type, metaDataEvent);

        this.metaDataEmitter.send(beanData);
    }

}
