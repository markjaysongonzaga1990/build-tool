package ent.cg.cmd.service.registry;

import ent.cg.boundary.Document;
import ent.cg.boundary.Registry;
import ent.cg.boundary.repository.RegistryRepository;
import ent.cg.core.enums.DOCUMENT_UNIT;
import ent.cg.core.event.BookletRegisterEvent;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Optional;

@Slf4j
@ApplicationScoped
public class BookletRegistryService {

    @Inject
    RegistryRepository registryRepository;

    @Transactional
    public Registry register(BookletRegisterEvent bookletRegisterEvent){
        log.info("{} :: {}",this.getClass().getSimpleName(),bookletRegisterEvent);

        var bookletCount = bookletRegisterEvent.getBookletCount();
        var sequencePcs = bookletRegisterEvent.getSequencePcs();
        var companyType = bookletRegisterEvent.getCompanyType();
        var documentType = bookletRegisterEvent.getDocumentType();

        var documentUnit = DOCUMENT_UNIT.BOOKLET;

        var lastRegistry = this.registryRepository.findLatest(companyType,documentType, documentUnit);

        var startingIndex = Optional.ofNullable(lastRegistry).isPresent() ?
                lastRegistry.getDocument().getEndSequence() : 0L;

        var registry = Registry.builder()
                .company(companyType)
                .documentType(documentType)
                .documentUnit(documentUnit)
                .input(bookletCount)
                .sequencePcs(sequencePcs)
                .document(Document.builder()
                        .startSequence(startingIndex + 1)
                        .endSequence((bookletCount * sequencePcs) + startingIndex)
                        .build())
                .encodedBy(bookletRegisterEvent.getUser())
                .build();

        this.registryRepository.persist(registry);

        return registry;
    }
}
