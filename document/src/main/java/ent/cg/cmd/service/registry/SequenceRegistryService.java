package ent.cg.cmd.service.registry;


import ent.cg.boundary.Document;
import ent.cg.boundary.Registry;
import ent.cg.boundary.repository.RegistryRepository;
import ent.cg.cmd.interceptor.overlap.Overlap;
import ent.cg.core.enums.DOCUMENT_UNIT;
import ent.cg.core.event.SequenceRegisterEvent;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@Slf4j
@ApplicationScoped
public class SequenceRegistryService {

    @Inject
    RegistryRepository registryRepository;

    @Overlap
    @Transactional
    public Registry register(SequenceRegisterEvent sequenceRegisterEvent) {
        log.info("{} :: {}", this.getClass().getSimpleName(), sequenceRegisterEvent);

        var companyType = sequenceRegisterEvent.getCompanyType();
        var documentType = sequenceRegisterEvent.getDocumentType();

        var sequenceFrom = sequenceRegisterEvent.getSequenceFrom();
        var sequenceTo = sequenceRegisterEvent.getSequenceTo();

        var documentUnit = DOCUMENT_UNIT.SEQUENCE;

        var registry = Registry.builder()
                .company(companyType)
                .documentType(documentType)
                .documentUnit(documentUnit)
                .sequenceFrom(Long.parseLong(sequenceFrom))
                .sequenceTo(Long.parseLong(sequenceTo))
                .document(Document.builder()
                        .startSequence(Long.parseLong(sequenceFrom))
                        .endSequence(Long.parseLong(sequenceTo))
                        .build())
                .encodedBy(sequenceRegisterEvent.getUser())
                .build();

        this.registryRepository.persist(registry);

        return registry;
    }
}
