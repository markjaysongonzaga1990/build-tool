package ent.cg.cmd.service.release;

import ent.cg.boundary.Approval;
import ent.cg.boundary.Assignment;
import ent.cg.boundary.Registry;
import ent.cg.boundary.Release;
import ent.cg.boundary.repository.RegistryRepository;
import ent.cg.boundary.repository.ReleaseRepository;
import ent.cg.core.enums.DOCUMENT_UNIT;
import ent.cg.core.enums.RELEASE_STATUS;
import ent.cg.core.event.SequenceReleaseEvent;
import io.quarkus.panache.common.Page;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Slf4j
@RequestScoped
public class SequenceReleaseService {

    @Inject
    JsonWebToken jsonWebToken;

    @Inject
    ReleaseRepository releaseRepository;

    @Inject
    RegistryRepository registryRepository;

    @Transactional
    public Release release(SequenceReleaseEvent sequenceReleaseEvent){
        var companyType = sequenceReleaseEvent.getCompanyType();
        var documentType = sequenceReleaseEvent.getDocumentType();
        var documentUnit = DOCUMENT_UNIT.SEQUENCE;
        var sequenceFrom = sequenceReleaseEvent.getSequenceFrom();
        var sequenceTo = sequenceReleaseEvent.getSequenceTo();

        var xFrom = Long.parseLong(sequenceFrom);
        var xTo = Long.parseLong(sequenceTo);

        var assignedPersonnelId = sequenceReleaseEvent.getAssignedPersonnelId();
        var branchOfficeId = sequenceReleaseEvent.getBranchOfficeId();

        var allReleases = this.releaseRepository.findAllReleasesPage(documentUnit, documentType, companyType, Page.ofSize(5))
                .stream()
                .sorted(Comparator.comparingLong(Release::getId).reversed())
                .findFirst();

        long registryId = allReleases.isPresent() ?  allReleases.get().getRegistry().getId() : 0L;

        var registryList = this.registryRepository.findAllRegistryStartingFromId(registryId,companyType,documentType);

        if(registryList.isEmpty()) throw new RuntimeException("""
                No Registry record found! Cannot proceed to releasing.
                Make sure that there is an available document to release first.
                """);
        //check the last registry if there is a remaining sequence to release .. else move to the next registry
        var registryToRelease = registryList.stream()
                .sorted(Comparator.comparingLong(Registry::getId))
                .map(Registry.class::cast)
                .filter(registration -> registration.getDocumentUnit().equals(DOCUMENT_UNIT.SEQUENCE))
                .filter(registry -> {
                    var id = registry.getId();
                    var from = registry.getSequenceFrom();
                    var to = registry.getSequenceTo();

                    //validation
                    var releases = this.releaseRepository.findAllReleaseByRegistryId(id);
                    var isValid = releases
                            .parallelStream()
                            .filter(x -> { //validate range
                                var xxFrom = x.getSequenceFrom();
                                var xxTo = x.getSequenceTo();

                                return true;
                            })
                            .findFirst().isPresent();

                    return releases.isEmpty() || isValid;
                }).findFirst();

        if(registryToRelease.isEmpty()){
            throw new RuntimeException("No Available Document to release. All booklets have been released already.");
        }

        var registry = registryToRelease.get();

        var assignments = LongStream.rangeClosed(xFrom, xTo)
                .mapToObj(sequenceNumber -> Assignment.builder()
                        .assignedPersonnelId(assignedPersonnelId)
                        .sequenceNumber(sequenceNumber)
                        .branchOfficeId(branchOfficeId)
                        .build())
                .collect(Collectors.toSet());

        var release = Release.builder()
                .assignments(assignments)
                .approval(Approval.builder()
                        .releaseStatus(RELEASE_STATUS.PENDING_APPROVAL)
                        .build())
                .releasedBy(UUID.fromString(jsonWebToken.getSubject()))
                .registry(registry)
                .sequenceFrom(xFrom)
                .sequenceTo(xTo)
                .build();
        this.releaseRepository.persist(release);

        return release;
    }
}
