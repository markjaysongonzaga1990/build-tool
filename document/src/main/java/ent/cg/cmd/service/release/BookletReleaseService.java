package ent.cg.cmd.service.release;

import ent.cg.boundary.*;
import ent.cg.boundary.repository.RegistryRepository;
import ent.cg.boundary.repository.ReleaseRepository;
import ent.cg.core.enums.DOCUMENT_UNIT;
import ent.cg.core.enums.RELEASE_STATUS;
import ent.cg.core.event.BookletReleaseEvent;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Comparator;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Slf4j
@RequestScoped
public class BookletReleaseService {

    @Inject
    RegistryRepository registryRepository;

    @Inject
    ReleaseRepository releaseRepository;

    @Inject
    JsonWebToken jsonWebToken;

    @Transactional
    public Release release(BookletReleaseEvent bookletReleaseEvent){
        var companyType = bookletReleaseEvent.getCompanyType();
        var documentType = bookletReleaseEvent.getDocumentType();
        var documentUnit = DOCUMENT_UNIT.BOOKLET;
        var bookletCount = bookletReleaseEvent.getBookletCount();

        var assignedPersonnelId = bookletReleaseEvent.getAssignedPersonnelId();
        var branchOfficeId = bookletReleaseEvent.getBranchOfficeId();

        var allReleases = this.releaseRepository.findAllReleases(documentUnit, documentType, companyType)
                .stream()
                .sorted(Comparator.comparingLong(Release::getId).reversed())
                .findFirst();

        long registryId = allReleases.isPresent() ?  allReleases.get().getRegistry().getId() : 0L;

        var registryList = this.registryRepository.findAllRegistryStartingFromId(registryId,companyType,documentType);

        if(registryList.isEmpty()) throw new RuntimeException("""
                No Registry record found! Cannot proceed to releasing.
                Make sure that there is an available document to release first.
                """);

        int totalReleased[] = {0};
        var registryToRelease = registryList.stream()
                .sorted(Comparator.comparingLong(Registry::getId))
                .map(Registry.class::cast)
                .filter(registration -> registration.getDocumentUnit().equals(DOCUMENT_UNIT.BOOKLET))
                .filter(e-> {
                    totalReleased[0] = 0; //reset
                    var isLastEncoded = e.getId() == registryId;
                    if(isLastEncoded){
                        totalReleased[0] = this.releaseRepository.findAllReleaseByRegistryId(registryId)
                                .stream()
                                .map(Release::getInput)
                                .reduce(0,Integer::sum);
                        //check if the current encoding record has remaining booklet
                        var hasNoRemaining = e.getInput() <=  totalReleased[0];
                        return isLastEncoded ? !hasNoRemaining : true;
                    }
                    return true;
                }).findFirst();

        if(registryToRelease.isEmpty()){
            throw new RuntimeException("No Available Document to release. All booklets have been released already.");
        }

        var registry = registryToRelease.get();
        var remaining = registry.getInput() - totalReleased[0];
        if(bookletCount > remaining && remaining > 0){
            throw new RuntimeException("The maximum booklet that you can release in this transaction is " + remaining +
                    ".Registry id : " + registry.getId() + " has " + totalReleased[0] + " out of " + registry.getInput() +
                    " issued.");
        }

        var sequencePcs = registry.getSequencePcs();
        var startSequence = (sequencePcs * totalReleased[0]) + 1;
        var endSequence = sequencePcs * bookletCount;

        var assignments = LongStream.rangeClosed(startSequence,endSequence)
                .mapToObj(sequenceNumber -> Assignment.builder()
                        .assignedPersonnelId(assignedPersonnelId)
                        .sequenceNumber(sequenceNumber)
                        .branchOfficeId(branchOfficeId)
                        .build())
                .collect(Collectors.toSet());

        var release = Release.builder()
                .assignments(assignments)
                .approval(Approval.builder()
                        .releaseStatus(RELEASE_STATUS.PENDING_APPROVAL)
                        .build())
                .releasedBy(UUID.fromString(jsonWebToken.getSubject()))
                .registry(registry)
                .input(bookletCount)
                .sequenceFrom(sequencePcs)
                .sequenceTo(endSequence)
                .build();
        this.releaseRepository.persist(release);

        return release;
    }
}
