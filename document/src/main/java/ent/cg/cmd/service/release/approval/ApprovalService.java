package ent.cg.cmd.service.release.approval;

import ent.cg.boundary.Approval;
import ent.cg.boundary.repository.ApprovalRepository;
import ent.cg.core.enums.RELEASE_STATUS;
import ent.cg.core.event.ApprovalEvent;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.UUID;

@Slf4j
@RequestScoped
public class ApprovalService {

    @Inject
    ApprovalRepository approvalRepository;

    @Inject
    JsonWebToken jsonWebToken;

    @Transactional
    public Approval approve(ApprovalEvent approvalEvent){
        var releaseId = approvalEvent.getId();
        var approve = this.approvalRepository.approve(releaseId)
                .orElseThrow(() -> new RuntimeException("Release Record not found!"));

        var release = approve.getRelease();

        var approval = Approval.builder()
                .releaseStatus(RELEASE_STATUS.APPROVED)
                .approvedBy(UUID.fromString(jsonWebToken.getSubject()))
                .release(release)
                .build();

        return approval;
    }
}
