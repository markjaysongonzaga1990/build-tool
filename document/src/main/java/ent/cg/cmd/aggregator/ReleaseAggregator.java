package ent.cg.cmd.aggregator;

import ent.cg.boundary.Release;
import ent.cg.cmd.interceptor.message.Message;
import ent.cg.cmd.service.release.BookletReleaseService;
import ent.cg.cmd.service.release.SequenceReleaseService;
import ent.cg.core.event.BookletReleaseEvent;
import ent.cg.core.event.SequenceReleaseEvent;
import io.quarkus.vertx.ConsumeEvent;
import io.smallrye.mutiny.Uni;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@Slf4j
@ApplicationScoped
public class ReleaseAggregator {

    @Inject
    BookletReleaseService bookletReleaseService;

    @Inject
    SequenceReleaseService sequenceReleaseService;

    @Message
    @ConsumeEvent(value = "bookletReleaseEvent")
    public Release on(BookletReleaseEvent bookletReleaseEvent){
        log.info("bookletReleaseEvent :: {}",bookletReleaseEvent);
        //handle registration by booklet
        return this.bookletReleaseService.release(bookletReleaseEvent);
    }

    @Message
    @ConsumeEvent(value = "sequenceReleaseEvent")
    public Release on(SequenceReleaseEvent sequenceReleaseEvent){
        log.info("sequenceReleaseEvent :: {}",sequenceReleaseEvent);
        //handle registration by sequence
       return this.sequenceReleaseService.release(sequenceReleaseEvent);
    }

}
