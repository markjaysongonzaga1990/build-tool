package ent.cg.cmd.aggregator;

import ent.cg.boundary.Registry;
import ent.cg.cmd.interceptor.message.Message;
import ent.cg.cmd.service.registry.BookletRegistryService;
import ent.cg.cmd.service.registry.SequenceRegistryService;
import ent.cg.core.event.BookletRegisterEvent;
import ent.cg.core.event.SequenceRegisterEvent;
import io.quarkus.vertx.ConsumeEvent;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@Slf4j
@ApplicationScoped
public class RegisterAggregator {

    @Inject
    BookletRegistryService bookletRegistryService;

    @Inject
    SequenceRegistryService sequenceRegistryService;

    @Message
    @ConsumeEvent(value = "bookletRegisterEvent",blocking = true)
    public Registry on(BookletRegisterEvent bookletRegisterEvent){
        log.info("bookletRegisterEvent :: {}",bookletRegisterEvent);
        //handle registration by booklet;
        return this.bookletRegistryService.register(bookletRegisterEvent);
    }

    @Message
    @ConsumeEvent(value = "sequenceRegisterEvent",blocking = true)
    public Registry on(SequenceRegisterEvent sequenceRegisterEvent){
        log.info("bookletRegisterEvent :: {}",sequenceRegisterEvent);
        //handle registration by sequence
        return this.sequenceRegistryService.register(sequenceRegisterEvent);
    }

}
