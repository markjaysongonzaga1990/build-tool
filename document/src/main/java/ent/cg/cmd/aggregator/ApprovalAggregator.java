package ent.cg.cmd.aggregator;

import ent.cg.boundary.Approval;
import ent.cg.cmd.interceptor.message.Message;
import ent.cg.cmd.service.release.approval.ApprovalService;
import ent.cg.core.event.ApprovalEvent;
import io.quarkus.vertx.ConsumeEvent;
import io.smallrye.mutiny.Uni;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@Slf4j
@ApplicationScoped
public class ApprovalAggregator {

    @Inject
    ApprovalService approvalService;

    @Message
    @ConsumeEvent(value = "approveReleaseEvent")
    public Approval on(ApprovalEvent approveReleaseEvent){
        log.info("approveReleaseEvent :: {}",approveReleaseEvent);
        return this.approvalService.approve(approveReleaseEvent);
    }

}
