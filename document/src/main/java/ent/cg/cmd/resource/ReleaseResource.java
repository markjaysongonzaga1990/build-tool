package ent.cg.cmd.resource;

import ent.cg.core.dto.ValidationResult;
import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.core.enums.DOCUMENT_UNIT;
import ent.cg.core.event.BookletReleaseEvent;
import ent.cg.core.event.SequenceReleaseEvent;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.core.eventbus.EventBus;

import javax.inject.Inject;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/release")
@Produces(value = MediaType.APPLICATION_JSON)
@Consumes(value = MediaType.APPLICATION_JSON)
public class ReleaseResource {
    @Inject
    EventBus eventBus;

    @Inject
    Validator validator;

    @POST
    @Path("/booklet")
    public Uni<Response> bookletReleaseEvent(final BookletReleaseEvent bookletReleaseEvent){
        var validateResult = this.validator.validate(bookletReleaseEvent);
        if(validateResult.isEmpty()){
            bookletReleaseEvent.setCompanyType(COMPANY_TYPE.COSMOS);
            this.eventBus.send("bookletReleaseEvent", bookletReleaseEvent);
            return Uni.createFrom().item(Response.accepted()
                    .build());
        }
        return Uni.createFrom().item(Response.status(400)
                .entity(new ValidationResult(validateResult)).build());
    }

    @POST
    @Path("/sequence")
    public Uni<Response> sequenceReleaseEvent(final SequenceReleaseEvent sequenceReleaseEvent){
        var validateResult = this.validator.validate(sequenceReleaseEvent);
        if(validateResult.isEmpty()){
            sequenceReleaseEvent.setCompanyType(COMPANY_TYPE.GOLDEN_FUTURE);
            sequenceReleaseEvent.setDocumentUnit(DOCUMENT_UNIT.SEQUENCE);
            this.eventBus.send("sequenceReleaseEvent", sequenceReleaseEvent);
            return Uni.createFrom().item(Response.accepted()
                    .build());
        }

        return Uni.createFrom().item(Response.status(400)
                .entity(new ValidationResult(validateResult)).build());
    }
}
