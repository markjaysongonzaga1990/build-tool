package ent.cg.cmd.resource;

import ent.cg.core.dto.ValidationResult;
import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.core.enums.DOCUMENT_UNIT;
import ent.cg.core.event.BookletRegisterEvent;
import ent.cg.core.event.SequenceRegisterEvent;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.core.eventbus.EventBus;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Path("/registration")
@RequestScoped
@Produces(value = MediaType.APPLICATION_JSON)
@Consumes(value = MediaType.APPLICATION_JSON)
public class RegistrationResource {

    @Inject
    JsonWebToken jwt;

    @Inject
    EventBus eventBus;

    @Inject
    Validator validator;

    @POST
    @Path("/booklet")
    public Uni<Response> bookletRegisterEvent(@DefaultValue("50") @QueryParam("bookletPcsCount") int bookletPcsCount,
                                         final BookletRegisterEvent bookletRegisterEvent){
        var validateResult = this.validator.validate(bookletRegisterEvent);
        if(validateResult.isEmpty()){
            bookletRegisterEvent.setCompanyType(COMPANY_TYPE.COSMOS);
            bookletRegisterEvent.setSequencePcs((long)bookletPcsCount);
            bookletRegisterEvent.setUser(UUID.fromString(jwt.getSubject()));
            this.eventBus.send("bookletRegisterEvent", bookletRegisterEvent);
            return Uni.createFrom().item(Response.accepted()
                    .build());
        }
        return Uni.createFrom().item(Response.status(400)
                .entity(new ValidationResult(validateResult)).build());
    }

    @POST
    @Path("/sequence")
    public Uni<Response> sequenceRegisterEvent(final SequenceRegisterEvent sequenceRegisterEvent){
        var validateResult = this.validator.validate(sequenceRegisterEvent);
        if(validateResult.isEmpty()){
            sequenceRegisterEvent.setCompanyType(COMPANY_TYPE.GOLDEN_FUTURE);
            sequenceRegisterEvent.setDocumentUnit(DOCUMENT_UNIT.SEQUENCE);
            sequenceRegisterEvent.setUser(UUID.fromString(jwt.getSubject()));
            this.eventBus.send("sequenceRegisterEvent", sequenceRegisterEvent);
            return Uni.createFrom().item(Response.accepted()
                    .build());
        }

        return Uni.createFrom().item(Response.status(400)
                .entity(new ValidationResult(validateResult)).build());
    }
}
