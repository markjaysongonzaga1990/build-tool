package ent.cg.cmd.resource;

import ent.cg.core.event.ApprovalEvent;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.core.eventbus.EventBus;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/approve")
@Produces(value = MediaType.APPLICATION_JSON)
@Consumes(value = MediaType.APPLICATION_JSON)
public class ApprovalResource {
    @Inject
    EventBus eventBus;

    @POST
    @Path("/{id}")
    public Uni<Response> approveReleaseEvent(@PathParam("id") Long id){
        this.eventBus.send("approveReleaseEvent", ApprovalEvent.builder()
                .id(id)
                .build());

        return Uni.createFrom().item(Response.accepted()
                .build());
    }
}
