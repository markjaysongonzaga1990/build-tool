package ent.cg.boundary;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SecondaryTables({
        @SecondaryTable(name = "booklet_release")
})
public class Release {
    @Id
    @GeneratedValue
    Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    Registry registry;

    @Column(nullable = false,table = "booklet_release")
    Integer input;

    Long sequenceFrom;

    Long sequenceTo;

    @OneToOne(cascade = CascadeType.ALL,mappedBy = "release")
    Approval approval;

    @OneToMany(cascade = CascadeType.ALL)
    Set<Assignment> assignments;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(nullable = false)
    UUID releasedBy;
}
