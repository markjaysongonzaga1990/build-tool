package ent.cg.boundary;

import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.core.enums.DOCUMENT_TYPE;
import ent.cg.core.enums.DOCUMENT_UNIT;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SecondaryTables({
        @SecondaryTable(name = "booklet_registry"),
        @SecondaryTable(name = "sequence_registry")
})
public class Registry {
    @Id
    @GeneratedValue
    Long id;

    @OneToMany(mappedBy = "registry")
    List<Release> releases;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    DOCUMENT_TYPE documentType;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    DOCUMENT_UNIT documentUnit;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    COMPANY_TYPE company;

    @Column(nullable = false,table = "booklet_registry")
    Integer input;

    @ColumnDefault("50")
    @Column(table = "booklet_registry")
    Long sequencePcs;

    @Column(table = "sequence_registry")
    Long sequenceFrom;

    @Column(table = "sequence_registry")
    Long sequenceTo;

    @OneToOne(cascade = CascadeType.ALL)
    Document document;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date createdDate;

    @Column(nullable = false)
    UUID encodedBy;
}
