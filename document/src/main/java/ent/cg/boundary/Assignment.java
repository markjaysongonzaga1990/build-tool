package ent.cg.boundary;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Assignment {
    @Id
    @GeneratedValue
    Long id;

    Long sequenceNumber;

    @Column(nullable = false)
    Long branchOfficeId;

    @Column(nullable = false)
    Long assignedPersonnelId;
}
