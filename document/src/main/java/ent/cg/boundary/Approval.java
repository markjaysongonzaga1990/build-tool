package ent.cg.boundary;

import ent.cg.core.enums.RELEASE_STATUS;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Approval {
    @Id
    @GeneratedValue
    Long id;

    @ManyToOne
    Release release;

    UUID approvedBy;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    RELEASE_STATUS releaseStatus;
}
