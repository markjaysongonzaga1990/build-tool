package ent.cg.boundary.repository;

import ent.cg.boundary.Registry;
import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.core.enums.DOCUMENT_TYPE;
import ent.cg.core.enums.DOCUMENT_UNIT;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import io.quarkus.panache.common.Sort;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class RegistryRepository implements PanacheRepository<Registry> {

    public List<Registry> findAllPageByCompanyAndType(COMPANY_TYPE company, DOCUMENT_TYPE documentType, Integer size, Integer page){
        var parameters = Parameters.with("company", company)
                .and("documentType",documentType);
        return find("FROM Registry e WHERE e.company =: company AND e.documentType =: documentType", parameters)
                .page(page,size).list();
    }

    public Registry findLatest(COMPANY_TYPE companyType, DOCUMENT_TYPE documentType, DOCUMENT_UNIT documentUnit){
        return this.findByUnitAndType(companyType, documentType, documentUnit)
                .stream()
                .findFirst().orElse(null);
    }

    public List<Registry> findByUnitAndType(COMPANY_TYPE companyType, DOCUMENT_TYPE documentType, DOCUMENT_UNIT documentUnit){
        return find("""
                        FROM Registry e 
                        WHERE e.documentType =:documentType AND 
                        company =:companyType AND 
                        documentUnit =:documentUnit
                        """,
                Sort.descending("id"),
                Parameters.with("documentType",documentType)
                        .and("companyType",companyType)
                        .and("documentUnit",documentUnit))
                .list();
    }

    public List<Registry> findAllRegistryStartingFromId(Long lastRegistryRecord,COMPANY_TYPE companyType,DOCUMENT_TYPE documentType) {
        var parameters = Parameters.with("documentType", documentType)
                .and("company", companyType)
                .and("lastRegistryRecord", lastRegistryRecord);
        return find("""
                FROM Registry WHERE documentType =: documentType and company =:company and id >=: lastRegistryRecord
                """, parameters)
                .list();
    }
}
