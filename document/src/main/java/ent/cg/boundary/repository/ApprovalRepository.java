package ent.cg.boundary.repository;

import ent.cg.boundary.Approval;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.Optional;

@ApplicationScoped
public class ApprovalRepository implements PanacheRepository<Approval> {

    @Transactional
    public Optional<Approval> approve(Long releaseId){
        return find("FROM Approval e WHERE e.release.id =: releaseId",
                Parameters.with("releaseId",releaseId))
                .firstResultOptional();
    }
}
