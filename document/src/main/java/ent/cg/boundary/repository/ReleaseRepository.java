package ent.cg.boundary.repository;

import ent.cg.boundary.Registry;
import ent.cg.boundary.Release;
import ent.cg.core.enums.COMPANY_TYPE;
import ent.cg.core.enums.DOCUMENT_TYPE;
import ent.cg.core.enums.DOCUMENT_UNIT;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Parameters;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class ReleaseRepository implements PanacheRepository<Release> {

    public List<Release> findAllReleases(DOCUMENT_UNIT documentUnit,
                                         DOCUMENT_TYPE documentType,
                                         COMPANY_TYPE company){

        var parameters = Parameters.with("documentUnit", documentUnit)
                .and("documentType", documentType)
                .and("company", company);
        return findAllRelease(parameters).list();
    }

    public List<Release> findAllReleasesPage(DOCUMENT_UNIT documentUnit,
                                             DOCUMENT_TYPE documentType,
                                             COMPANY_TYPE company, Page page){

        var parameters = Parameters.with("documentUnit", documentUnit)
                .and("documentType", documentType)
                .and("company", company);
        return findAllRelease(parameters)
                .page(page).list();
    }

    private PanacheQuery<Release> findAllRelease(Parameters parameters) {
        return find("""
                        FROM Release e WHERE e.registry.documentUnit =:documentUnit AND 
                        e.registry.documentType =:documentType AND
                        e.registry.company =:company ORDER BY 
                        e.registry.id DESC
                        """, parameters);
    }

    public List<Release> findAllReleaseByRegistry(Registry currentRegistry) {
        var parameters = Parameters.with("currentRegistry", currentRegistry);
        return find("""
                FROM Release e WHERE e.registry =:currentRegistry 
                """, parameters).list();
    }

    public List<Release> findAllReleaseByRegistryId(long registryId) {
        var parameters = Parameters.with("registryId", registryId);
        return find("""
                FROM Release e WHERE e.registry.id =:registryId 
                """, parameters).list();
    }

}
